#!/usr/bin/env python
#Boa:App:BoaApp

from wxPython.wx import *
from wxUtils import *

import ScriptManagerFrame

modules ={u'InnoSetup': [0, '', u'iwBuilder/InnoSetup.py'],
 u'Misc': [0, '', u'iwBuilder/Misc.py'],
 u'OptionsDialog': [0, '', u'OptionsDialog.py'],
 u'Publish': [0, '', u'iwBuilder/Publish.py'],
 u'Script': [0, '', u'iwBuilder/Script.py'],
 u'ScriptManagerFrame': [1,
                         'Main frame of Application',
                         u'ScriptManagerFrame.py'],
 u'VisualC': [0, '', u'iwBuilder/VisualC.py'],
 u'Web': [0, '', u'iwBuilder/Web.py'],
 u'cvs': [0, '', u'iwBuilder/cvs.py'],
 u'iwCmd': [0, '', u'iwBuilder/iwCmd.py'],
 u'process': [0, '', u'iwBuilder/process.py'],
 u'svn': [0, '', u'iwBuilder/svn.py']}

class BoaApp(wxApp):
    def OnInit(self):
        installErrOutLoggers()
        wxInitAllImageHandlers()
        self.SetAppName('ScriptManager')
        self.SetVendorName('SoftArchi')
        self.config = wxConfig()
        wxConfigBase_Set(self.config)

        self.main = ScriptManagerFrame.create(None)
        self.main.Center()
        self.main.Show()
        self.SetTopWindow(self.main)
        return True

    def OnExit(self):
        uninstallErrOutLoggers()

def main():
    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
