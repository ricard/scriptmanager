from wxPython.wx import *
import sys

padWidth = 80
pad = padWidth*' '

class PseudoFile:
    """ Base class for file like objects to facilitate StdOut for the Shell."""
    def __init__(self, output = None):
        if output is None: output = []
        self.output = output

    def writelines(self, l):
        map(self.write, l)

    def write(self, s):
        pass

    def flush(self):
        pass

    def isatty(self):
        return false

class PseudoFileOutStore(PseudoFile):
    """ File like obj with list storage """
    def write(self, s):
        self.output.append(s)

    def read(self):
        return ''.join(self.output)


class LoggerPF(PseudoFile):
    """ Base class for logging file like objects """
    def pad(self, s):
        padded = s + pad
        return padded[:padWidth] + padded[padWidth:].strip()

class OutputLoggerPF(LoggerPF):
    """ Logs stdout to wxLog functions"""
    def write(self, s):
        if s.strip():
##            if Preferences.recordModuleCallPoint:
##                frame = get_current_frame()
##                ss = s.strip()+ ' : <<%s, %d>>' % (
##                     frame.f_back.f_code.co_filename,
##                     frame.f_back.f_lineno,)
##            else:
            ss = s
            wxLogMessage(self.pad(ss).replace('%', '%%'))

        sys.__stdout__.write(s)

# XXX Should try to recognise warnings
# Match start against [v for k, v in __builtins__.items() if type(v) is types.ClassType and issubclass(v, Warning)]

class ErrorLoggerPF(LoggerPF):
    """ Logs stderr to wxLog functions"""
    def write(self, s):
        if not hasattr(self, 'buffer'):
            self.buffer = ''

        if s == '    ':
            self.buffer = s
        elif s[-1] != '\n':
            self.buffer = self.buffer + s
        else:
            wxLogError(self.pad(self.buffer+s[:-1]).replace('%', '%%'))

        sys.__stderr__.write(s)

def installErrOutLoggers():
    sys.stdout = OutputLoggerPF()
    sys.stderr = ErrorLoggerPF()

def uninstallErrOutLoggers():
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__
