import os
import string
import time
import win32api
from iwCmd import *
import xml

def InstantiateVisualC(workspace):
    if workspace.lower().endswith('.vcproj'):
        dom = xml.dom.minidom.parse(workspace)
        node = dom.documentElement
        if node.tagName != 'VisualStudioProject':
            raise IOError(0, 'Bad project file format', workspace)
        version = node.getAttribute('Version')
        if version == "7.10" or version == "7,10":
            return VisualC7(workspace)
        elif version == "8,00" or version == "8.00":
            return MSBuild(workspace)
        raise IOError(0, 'Bad project file version', workspace)
    elif  workspace.lower().endswith('.csproj'):
        return VisualCSharp8(workspace)
    elif  workspace.lower().endswith('.sln'):
        fd = open(workspace, 'rt')
        line = fd.readline().strip()
        if len(line) < 10:   # may be the second line
            line = fd.readline().strip()
        fd.close()
        print line
        if line.endswith('Version 8.00'):
            return VisualC7(workspace)
        elif line.endswith('Version 9.00') or line.endswith('Version 10.00'):
            return VisualC8(workspace)
        raise IOError(0, 'Bad project file version', workspace)
    else:
        return VisualC(workspace)

class VisualC(Cmd):
    def __init__(self, workspace):
        """Creates the Visual C Compiler wrapper class.

           To find the Visual C++ executable, this class looks for the 'MSDevDir'
           environnement variable."""
        Cmd.__init__(self)
        if not os.path.exists(workspace):
            raise IOError(0, 'Project file not found', workspace)
        (self._path, self._workspace) = os.path.split(workspace)
        Cmd.set_path(self, self._path)
        try:
            self.__msdev = os.path.join(os.environ['MSDevDir'], 'bin', 'msdev.exe')
        except KeyError:
            self.__msdev = os.path.join('C:', 'Program Files', 'Microsoft Visual Studio', 'Common', 'MSDev98', 'Bin', 'msdev.exe')

    def make(self, project, config, isMakeAll = False, isNoDep = False):
        """Execute an 'MAKE' command."""
        args = ['"' + self.__msdev + '"', '"' + os.path.join(self._path, self._workspace) + '"', '/MAKE']
        args += ['"' + project + ' - Win32 ' + config + '"']
        if isMakeAll:
            args += ['/REBUILD']
        if isNoDep:
            args += ['/NORECURSE']
        return Cmd.run_cmd(self, args)

    def clean(self, project, config, isNoDep = False):
        """Execute an 'MAKE' command."""
        args = ['"' + self.__msdev + '"', '"' + os.path.join(self._path, self._workspace) + '"', '/MAKE']
        args += ['"' + project + ' - Win32 ' + config + '"']
        args += ['/CLEAN']
        if isNoDep:
            args += ['/NORECURSE']
        return Cmd.run_cmd(self, args)

    def incredibuild(self, project, config, isMakeAll = False, isNoDep = False, extraCompilOpt = None):
        """Execute an 'MAKE' command for the solution and the given config with IncrediBuild."""
        args = ['BuildConsole "' + os.path.join(self._path, self._workspace) + '"']
        args += ['/prj="' + project + '"']
        args += ['/cfg="Win32 ' + config + '"']
        if extraCompilOpt:
            args += ['/cl_add="' + extraCompilOpt + '" ']
        if isMakeAll:
            args += ['/rebuild']
        if isNoDep:
            args += ['/NoRecurse']
        return Cmd.run_cmd(self, args)

    def get_version(self, project):
        name, ext = os.path.splitext(self._workspace)
        if ext.upper() == '.DSP':
            projectFile = self._workspace
        else:
            projectFile = self.get_project_file(project)
        if projectFile == None:
            sys.stderr.write("Can't find project named '" + project + "'\n")
            return
        fullProjectFilename = os.path.join(self._path, projectFile)
        fd = file(fullProjectFilename, 'rt')
        lines = fd.readlines()
        fd.close()
        (projectPath, projectFilename) = os.path.split(fullProjectFilename)
        try:
            (line, index) = find_line(project + '.rc', lines, case_sensitive = False)
            if line != None:
                str = lines[line]
                i1 = str.index('=') + 1
                i2 = str.index('\n')
                rcFile = str[i1:i2]
                return VersionInfo(os.path.join(projectPath, rcFile))
            else:
                sys.stderr.write("Can't find ressource file named '" + project + ".rc'\n")
        except ValueError:
            sys.stderr.write("Can't find resources file named '" + project + ".rc'\n")
            raise

    def get_project_file(self, project):
        fd = file(os.path.join(self._path, self._workspace), 'rt')
        lines = fd.readlines()
        fd.close()
        line = 0
        while True:
            try:
                (line, index) = find_line('Project:', lines, line)
                if line == None:
                    break;
                str = lines[line]
                i1 = str.index('"')
                i2 = str.index('"', i1 + 1)
                projectName = str[i1 + 1 : i2]
                if project == projectName:
                    i1 = str.index('=', i2) + 1
                    if str[i1] == '"':
                        i1 += 1
                    i2 = str.upper().index('.DSP', i1) + 4
                    projectFile = str[i1:i2]
                    return projectFile
                line += 1
            except:
                pass

class VisualC7(Cmd):
    def __init__(self, workspace):
        """Creates the Visual C Compiler wrapper class.

           To find the Visual C++ executable, this class looks for the 'DevEnvDir'
           environnement variable."""
        Cmd.__init__(self)
        (self._path, self._workspace) = os.path.split(workspace)
        Cmd.set_path(self, self._path)
        try:
            self.__msdev = os.environ['DevEnvDir'] + '\\bin\\msdev.com'
        except:
            self.__msdev = 'C:/Program Files/Microsoft Visual Studio .NET 2003/Common7/IDE/DevEnv.com'

    def make(self, project, config, isMakeAll = False, isNoDep = False):
        """Execute an 'MAKE' command for the solution and the given config."""
        args = ['"' + self.__msdev + '"', '"' + os.path.join(self._path, self._workspace) + '"']
        if isMakeAll:
            args += ['/rebuild']
        else:
            args += ['/build']
        args += ['"' + config + '"']
        if project:
            args += ['/project ']
            args += ['"' + project + '"']
        if isNoDep:
            sys.stderr.write('isNoDep isn\'t supported by Visual7\n')
        return Cmd.run_cmd(self, args)

    def clean(self, project, config, isNoDep = False):
        """Execute an 'MAKE' command for the solution and the given config."""
        args = ['"' + self.__msdev + '"', '"' + os.path.join(self._path, self._workspace) + '"']
        args += ['/clean']
        args += ['"' + config + '"']
        if project:
            args += ['/project ']
            args += ['"' + project + '"']
        if isNoDep:
            sys.stderr.write('isNoDep isn\'t supported by Visual7\n')
        return Cmd.run_cmd(self, args)

    def incredibuild(self, project, config, isMakeAll = False, isNoDep = False, extraCompilOpt = None):
        """Execute an 'MAKE' command for the solution and the given config with IncrediBuild."""
        args = ['BuildConsole "' + os.path.join(self._path, self._workspace) + '"']
        args += ['/prj="' + project + '"']
        args += ['/cfg="' + config + '"']
        if extraCompilOpt:
            args += ['/cl_add="' + extraCompilOpt + '" ']
        if isMakeAll:
            args += ['/rebuild']
        if isNoDep:
            args += ['/NoRecurse']
        return Cmd.run_cmd(self, args)

    def get_version(self, project):
        name, ext = os.path.splitext(self._workspace)
        if ext.upper() == '.VCPROJ':
            projectFile = self._workspace
        else:
            projectFile = self.get_project_file(project)
        if projectFile == None:
            sys.stderr.write("Can't find project named '" + project + "'\n")
            return
        fullProjectFilename = os.path.join(self._path, projectFile)
        fd = file(fullProjectFilename, 'rt')
        lines = fd.readlines()
        fd.close()
        (projectPath, projectFilename) = os.path.split(fullProjectFilename)
        try:
            (line, index) = find_line(project + '.rc', lines, 0, False)
            if line != None:
                str = lines[line]
                i1 = str.index('"') + 1
                i2 = str.index('"', i1)
                rcFile = str[i1:i2]
                return VersionInfo(os.path.join(projectPath, rcFile))
            else:
                sys.stderr.write("Can't find ressource file named '" + project + ".rc'\n")
        except ValueError:
            sys.stderr.write("Can't find resources file named '" + project + ".rc'\n")
            raise

    def get_project_file(self, project):
        fd = file(os.path.join(self._path, self._workspace), 'rt')
        lines = fd.readlines()
        fd.close()
        line = 0
        while True:
            try:
                (line, index) = find_line('Project(', lines, line)
                if line == None:
                    break;
                str = lines[line]
                i1 = str.index('=')
                i1 = str.index('"', i1 + 1)
                i2 = str.index('"', i1 + 1)
                projectName = str[i1 + 1 : i2]
                if project.upper() == projectName.upper():
                    i1 = str.index('"', i2 + 1)
                    i2 = str.index('"', i1 + 1)
                    projectFile = str[i1 + 1 : i2]
                    return projectFile
                line += 1
            except:
                pass

class VersionInfo:
    """Accessor to the VS_VERSION_INFO section of a resources file."""

    def __init__(self, rcFile):
        """Constructor with resources files as parameter."""
        self._rcFile = rcFile
        self.fileVersion = [0, 0, 0, 0]
        self.productVersion = [0, 0, 0, 0]
        self.read_version()

    def read_version(self):
        """Reads version information from resources file."""
        fd = file(self._rcFile, 'rt')
        rc = fd.read()
        index = rc.find('VS_VERSION_INFO')
        if index >= 0:
            i1 = rc.find('FILEVERSION', index)
            if i1 >= 0:
                i1 += 12
                i2 = rc.find('\n', i1)
                ver = rc[i1:i2].split(',', 4)
                self.fileVersion = [int(ver[0]), int(ver[1]), int(ver[2]), int(ver[3])]
            i1 = rc.find('PRODUCTVERSION', index)
            if i1 >= 0:
                i1 += 15
                i2 = rc.find('\n', i1)
                ver = rc[i1:i2].split(',', 4)
                self.productVersion = [int(ver[0]), int(ver[1]), int(ver[2]), int(ver[3])]
        fd.close()

    def write_version(self):
        """Write new version information to the resources file."""
        fd = file(self._rcFile, 'rt')
        lines = fd.readlines()
        fd.close()
        (baseline, index) = find_line('VS_VERSION_INFO', lines)
        if baseline != None:
                # File version value
            (line, index) = find_line('FILEVERSION', lines, baseline)
            if line != None:
                lines[line] = lines[line][:index] + 'FILEVERSION %(maj)d,%(min)d,%(rel)d,%(build)d\n' \
                                                        %  {'maj' : self.fileVersion[0], \
                                                            'min' : self.fileVersion[1], \
                                                            'rel' : self.fileVersion[2], \
                                                            'build' : self.fileVersion[3]}
            else:
                sys.stderr.write("Can't find FILEVERSION in '" + self._rcFile + "'\n")

            # Product version value
            (line, index) = find_line('PRODUCTVERSION', lines, baseline)
            if line != None:
                lines[line] = lines[line][:index] + 'PRODUCTVERSION %(maj)d,%(min)d,%(rel)d,%(build)d\n' \
                                                        %  {'maj' : self.productVersion[0], \
                                                            'min' : self.productVersion[1], \
                                                            'rel' : self.productVersion[2], \
                                                            'build' : self.productVersion[3]}
            else:
                sys.stderr.write("Can't find PRODUCTVERSION in '" + self._rcFile + "'\n")
            # File version string
            (line, index) = find_line('FileVersion', lines, baseline)
            if line != None:
                lines[line] = lines[line][:index] + 'FileVersion", "%(maj)d.%(min)d.%(rel)d.%(build)d\\0"\n'\
                                                        %  {'maj' : self.fileVersion[0], \
                                                            'min' : self.fileVersion[1], \
                                                            'rel' : self.fileVersion[2], \
                                                            'build' : self.fileVersion[3]}
            else:
                sys.stderr.write("Can't find FileVersion in '" + self._rcFile + "'\n")
            # Product version string
            (line, index) = find_line('ProductVersion', lines, baseline)
            if line != None:
                lines[line] = lines[line][:index] + 'ProductVersion", "%(maj)d.%(min)d.%(rel)d.%(build)d\\0"\n'\
                                                        %  {'maj' : self.productVersion[0], \
                                                            'min' : self.productVersion[1], \
                                                            'rel' : self.productVersion[2], \
                                                            'build' : self.productVersion[3]}
            else:
                sys.stderr.write("Can't find ProductVersion in '" + self._rcFile + "'\n")
            # Private build string
            (line, index) = find_line('PrivateBuild', lines, baseline)
            if line != None:
                lines[line] = lines[line][:index] + 'PrivateBuild", "%(build)d\\0"\n' % {'build': self.productVersion[3]}
            # Special build string
            (line, index) = find_line('SpecialBuild', lines, baseline)
            if line != None:
                lines[line] = lines[line][:index] + 'SpecialBuild", "%(build)d\\0"\n' % {'build': self.productVersion[3]}
            # Compile Date string
            (line, index) = find_line('Compile Date', lines, baseline)
            if line != None:
                lines[line] = lines[line][:index] + 'Compile Date", "' + time.strftime('%c') + '\\0"\n'

            # Save all lines
            fd = file(self._rcFile, 'wt')
            fd.writelines(lines)
            #for str in lines:
            #    fd.write(str + '\n')
            #fd.close()
        else:
            sys.stderr.write("Can't find VS_VERSION_INFO in '" + self._rcFile + "'\n")

    def inc_major(self):
        self.productVersion[0] += 1
        self.productVersion[1] = 0
        self.productVersion[2] = 0
        self.productVersion[3] = 0

    def inc_minor(self):
        self.productVersion[1] += 1
        self.productVersion[2] = 0
        self.productVersion[3] = 0

    def inc_release(self):
        self.productVersion[2] += 1
        self.productVersion[3] += 1

    def inc_build(self):
        self.productVersion[3] += 1

    def __getitem__(self, index):
        if index < 0 or index >= 4:
            raise IndexError, index
        return self.productVersion[index]

    def __setitem__(self, index, value):
        if index < 0 or index >= 4:
            raise IndexError, index
        self.productVersion[index] = value

    def __str__(self):
        return '%d.%d.%d.%d' % (self.productVersion[0], self.productVersion[1], self.productVersion[2], self.productVersion[3])

def find_line(str, lines, starting = 0, case_sensitive = True):
    """Find un a string array the string with specified pattern.
       Returns a Turple with the line index (begining at 0) and the position
       of the wanted string. If not found, returns (None, None)."""
    i = starting
    for line in lines[starting:]:
        if case_sensitive:
            index = line.find(str)
        else:
            index = line.lower().find(str.lower())
        if index >= 0:
            return (i, index)
        i += 1
    return (None, None)

class MSBuild(Cmd):
    def __init__(self, workspace):
        """Creates the MSBuild wrapper class.

           To find the MSBuild executable, this class looks for the '.NET 2.0'
           framework."""
        Cmd.__init__(self)
        (self._path, self._workspace) = os.path.split(workspace)
        Cmd.set_path(self, self._path)
        dotNetBaseFolder = os.path.join(win32api.GetWindowsDirectory(), 'Microsoft.NET', 'Framework')
        dotNet2 = os.path.join(dotNetBaseFolder, 'v2.0.50727', 'MSBuild.exe')
        dotNet3 = os.path.join(dotNetBaseFolder, 'v3.0', 'MSBuild.exe')
        dotNet3_5 = os.path.join(dotNetBaseFolder, 'v3.5', 'MSBuild.exe')
        if not os.path.exists(dotNetBaseFolder):
            raise EnvironmentError(0, '.NET Framework not installed on this system.')
        elif os.path.exists(dotNet3_5):
            self.__msbuild = dotNet3_5
        elif os.path.exists(dotNet3):
            self.__msbuild = dotNet3
        elif os.path.exists(dotNet2):
            self.__msbuild = dotNet2
        else:
            raise EnvironmentError(0, 'At least .NET Framework v2.0 should be installed on this system.')
            
        try:
            self.__msdev = os.environ['DevEnvDir'] + '\\bin\\msdev.com'
        except:
            self.__msdev = 'C:/Program Files/Microsoft Visual Studio .NET 2003/Common7/IDE/DevEnv.com'

    def make(self, project, config, isMakeAll = False, isNoDep = False):
        """Execute an 'MAKE' command for the solution and the given config."""
        args = ['"' + self.__msbuild + '"', '/verbosity:minimal']
        if isMakeAll:
            args += ['/target:Rebuild']
        else:
            args += ['/target:Build']
        args += ['/p:Configuration="' + config + '"']
        if project:
            sys.stderr.write('Specification of a project isn\'t supported by MSBuild\n')

        if isNoDep:
            sys.stderr.write('isNoDep isn\'t supported by MSBuild\n')
        args += ['"' + os.path.join(self._path, self._workspace) + '"']
        return Cmd.run_cmd(self, args)

    def clean(self, project, config, isNoDep = False):
        """Execute an 'MAKE' command for the solution and the given config."""
        args = ['"' + self.__msbuild + '"', '/verbosity:minimal']
        args += ['/target:Clean']
        args += ['/p:Configuration="' + config + '"']
        if project:
            sys.stderr.write('Specification of a project isn\'t supported by MSBuild\n')
        if isNoDep:
            sys.stderr.write('isNoDep isn\'t supported by MSBuild\n')
        args += ['"' + os.path.join(self._path, self._workspace) + '"']
        return Cmd.run_cmd(self, args)

    def incredibuild(self, project, config, isMakeAll = False, isNoDep = False, extraCompilOpt = None):
        """Execute an 'MAKE' command for the solution and the given config with IncrediBuild."""
        sys.stderr.write('Incredibuild isn\'t supported by MSBuild. Using normal MSBuild.\n')
        return make(project, config, isMakeAll, isNoDep)
        #args = ['BuildConsole "' + os.path.join(self._path, self._workspace) + '"']
        #args += ['/prj="' + project + '"']
        #args += ['/cfg="' + config + '"']
        #if extraCompilOpt:
            #args += ['/cl_add="' + extraCompilOpt + '" ']
        #if isMakeAll:
            #args += ['/rebuild']
        #if isNoDep:
            #args += ['/NoRecurse']
        #return Cmd.run_cmd(self, args)

    def get_project_file(self, project):
        fd = file(os.path.join(self._path, self._workspace), 'rt')
        lines = fd.readlines()
        fd.close()
        line = 0
        while True:
            try:
                (line, index) = find_line('Project(', lines, line)
                if line == None:
                    break;
                str = lines[line]
                i1 = str.index('=')
                i1 = str.index('"', i1 + 1)
                i2 = str.index('"', i1 + 1)
                projectName = str[i1 + 1 : i2]
                if project.upper() == projectName.upper():
                    i1 = str.index('"', i2 + 1)
                    i2 = str.index('"', i1 + 1)
                    projectFile = str[i1 + 1 : i2]
                    return projectFile
                line += 1
            except:
                pass



class VisualC8(MSBuild):
    def get_version(self, project):
        name, ext = os.path.splitext(self._workspace)
        if ext.upper() == '.VCPROJ':
            projectFile = self._workspace
        else:
            projectFile = self.get_project_file(project)
        if projectFile == None:
            sys.stderr.write("Can't find project named '" + project + "'\n")
            return
        fullProjectFilename = os.path.join(self._path, projectFile)
        fd = file(fullProjectFilename, 'rt')
        lines = fd.readlines()
        fd.close()
        (projectPath, projectFilename) = os.path.split(fullProjectFilename)
        try:
            (line, index) = find_line(project + '.rc', lines, 0, False)
            if line != None:
                str = lines[line]
                i1 = str.index('"') + 1
                i2 = str.index('"', i1)
                rcFile = str[i1:i2]
                return VersionInfo(os.path.join(projectPath, rcFile))
            else:
                sys.stderr.write("Can't find ressource file named '" + project + ".rc'\n")
        except ValueError:
            sys.stderr.write("Can't find resources file named '" + project + ".rc'\n")
            raise


class VisualCSharp8(MSBuild):
    def get_version(self, project):
        name, ext = os.path.splitext(self._workspace)
        if ext.upper() == '.CSPROJ':
            projectFile = self._workspace
        else:
            projectFile = self.get_project_file(project)
        if projectFile == None:
            sys.stderr.write("Can't find project named '" + project + "'\n")
            return
        fullProjectFilename = os.path.join(self._path, projectFile)
        fd = file(fullProjectFilename, 'rt')
        lines = fd.readlines()
        fd.close()
        (projectPath, projectFilename) = os.path.split(fullProjectFilename)
        try:
            (line, index) = find_line('AssemblyInfo.cs', lines, 0, False)
            if line != None:
                str = lines[line]
                i1 = str.index('="') + 2
                i2 = str.index('"', i1)
                assemblyInfoFile = str[i1:i2]
                return AssemblyInfo(os.path.join(projectPath, assemblyInfoFile))
            else:
                sys.stderr.write("Can't find ressource file named 'AssemblyInfo.cs'\n")
        except ValueError:
            sys.stderr.write("Can't find resources file named 'AssemblyInfo.cs'\n")
            raise
    
def safeInt(txt):
    try:
        return int(txt)
    except:
        val = ''
        for c in txt:
            if c >= '0' and c <= '9':
                val += c
            else:
                break
        if len(val) > 0:
            return int(val)
        else:
            return 0
    
    
class AssemblyInfo(VersionInfo):
    """Accessor to the AssemblyInfo.cs file of a C# project."""

    def read_version(self):
        """Reads version information from resources file."""
        fd = file(self._rcFile, 'rt')
        lines = fd.readlines()
        for line in lines:
            if line.startswith('[assembly: AssemblyVersion("'):
                index = line.find('("') + 2
                ver = line[index:].split('.', 4)
                self.productVersion = [safeInt(ver[0]), safeInt(ver[1]), safeInt(ver[2]), safeInt(ver[3])]
                continue
            if line.startswith('[assembly: AssemblyFileVersion("'):
                index = line.find('("') + 2
                ver = line[index:].split('.', 4)
                self.fileVersion = [safeInt(ver[0]), safeInt(ver[1]), safeInt(ver[2]), safeInt(ver[3])]
                continue
                
        
    def write_version(self):
        """Write new version information to the resources file."""
        fd = file(self._rcFile, 'rt')
        lines = fd.readlines()
        fd.close()
        newLines = []
        for line in lines:
            if line.startswith('[assembly: AssemblyVersion("'):
                line = '[assembly: AssemblyVersion("%(maj)d.%(min)d.%(rel)d.%(build)d")]\n' \
                                                         % {'maj' : self.productVersion[0], \
                                                            'min' : self.productVersion[1], \
                                                            'rel' : self.productVersion[2], \
                                                            'build' : self.productVersion[3]}
            if line.startswith('[assembly: AssemblyFileVersion("'):
                line = '[assembly: AssemblyFileVersion("%(maj)d.%(min)d.%(rel)d.%(build)d")]\n' \
                                                         % {'maj' : self.fileVersion[0], \
                                                            'min' : self.fileVersion[1], \
                                                            'rel' : self.fileVersion[2], \
                                                            'build' : self.fileVersion[3]}
            newLines.append(line)
        fd = file(self._rcFile, 'wt')
        fd.writelines(newLines)
        fd.close()

