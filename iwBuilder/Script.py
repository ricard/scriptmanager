#!/usr/bin/env python
# pylint: disable-msg=R0902, R0912, R0914,R0915, W0706


import os
import sys
import time
import xml.dom.minidom
from distutils.file_util import copy_file
from iwCmd import Cmd
from VisualC import InstantiateVisualC
from cvs import cvs
from svn import svn
from InnoSetup import InnoSetup
from Misc import version_format
from Web import PublishHTML
from zipfile import ZipFile, ZIP_DEFLATED
import tempfile
#from exceptions import Exception

class BuildError(Exception):
    def __init__(self, description = None):
        Exception.__init__(self)
        self.description = description

    def __str__(self):
        return repr(self.description)

class BadReturnedValue(BuildError):
    def __init__(self, value, cmd = 'Unknown command'):
        BuildError.__init__(self, 'Bad returned value (%d) by command: [ %s ]' % (value, repr(cmd)))

class BadScriptFormat(BuildError):
    def __init__(self, details):
        BuildError.__init__(self, 'Bad script format: ' + details)

class BreakRequested(BuildError):
    def __init__(self):
        BuildError.__init__(self, 'Break requested by user')


_steps = (\
    'getSources',        \
    'incProductVersion',        \
    'updateProductVersion',\
    'updateFileVersion',\
    'build',      \
    'tests',      \
    'protection',         \
    'makeInstall', \
    'commitSources',     \
    'tagSources',     \
    'publish',            \
    )

def getText(nodelist):
    text = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            text = text + node.data
    return text

class Project:       #IGNORE:R0903
    """Describs a project to build."""
    def __init__(self, element):
        self.name = element.getAttribute('name')
        node = element.getElementsByTagName('file')[0]
        self.prjFile = os.path.expandvars( getText(node.childNodes) )
        nodes = element.getElementsByTagName('target')
        if len(nodes) > 0:
            self.target = getText(nodes[0].childNodes)
        else:
            self.target = ''
        node = element.getElementsByTagName('config')[0]
        self.config = getText(node.childNodes)
        nodes = element.getElementsByTagName('pdb')
        if len(nodes) > 0:
            self.pdb = os.path.expandvars( getText(nodes[0].childNodes) )
        else:
            self.pdb = ''

class Setup:         #IGNORE:R0903
    """Describs a setup to build."""
    def __init__(self, element):
        self.name = element.getAttribute('name')
        node = element.getElementsByTagName('script')[0]
        self.script = os.path.expandvars( getText(node.childNodes) )


class Script:        #IGNORE:R0902
    def __init__(self, xmlFile, authentificationCallback = None):    #IGNORE:R0902, R0912, R0914,R0915
        self.authentificationCallback = authentificationCallback
        self.curCmd = None
        self.should_stop = False
        self.oldStdout = sys.stdout
        self.logFile = None
        dom = xml.dom.minidom.parse(xmlFile)
        self.projects = {}
        self.setups = {}
        self.steps = {}
        builder = dom.documentElement
        self.name = builder.getAttribute('name')
        self.mainProject = builder.getAttribute('mainProject')
        self.mainSetup = builder.getAttribute('mainSetup')
        self.htmlTemplate = builder.getAttribute('htmlTemplate')
        self.scriptVersion = builder.getAttribute('scriptVersion')
        if self.scriptVersion == '' or self.scriptVersion == '1':
            self.LoadVersion(builder)
        else:
            self.LoadVersion(builder)


    def LoadRepositoryModuleList(self, allCVS):
        """Load module list for a each repository node in XML file.
        'allCVS' is the input XML node.
        """
        cvsGetList = []
        for cvsNode in allCVS:
            cvspath = os.path.expandvars(cvsNode.getAttribute('path'))
            modules = cvsNode.getElementsByTagName('module')
            moduleList = []
            for moduleNode in modules:
                moduleList.append(getText(moduleNode.childNodes))
            cvsGetList.append((cvspath, moduleList, cvsNode.nodeName))
        return cvsGetList

    def LoadSourceRepositories(self, nodes, stepName):
        getSources = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = stepName
            if getSources.has_key(name):
                raise NameError, 'Name [%s] already used in %s steps' % (name, stepName)
            allCVS = node.getElementsByTagName('cvs')
            getSources[name] = self.LoadRepositoryModuleList(allCVS)
            allSVN = node.getElementsByTagName('svn')
            getSources[name].extend(self.LoadRepositoryModuleList(allSVN))
        return getSources

    def LoadVersion(self, builder):
        nodes = builder.getElementsByTagName('env')
        if len(nodes):
            node = nodes[0]
            allVariables = node.getElementsByTagName('var')
            self.env = {}
            for var in allVariables:
                value = os.path.expandvars(getText(var.childNodes))
                self.env[var.getAttribute('name')] = value
                os.environ[var.getAttribute('name')] = value


        nodes = builder.getElementsByTagName('projects')
        if len(nodes) > 0:
            projectsElem = nodes[0]
            allProjects = projectsElem.getElementsByTagName('project')
            for prj in allProjects:
                self.projects[prj.getAttribute('name')] = Project(prj)
            if self.projects[self.mainProject] == None:
                raise BadScriptFormat('Main project is missing!')

        nodes = builder.getElementsByTagName('setups')
        if len(nodes) > 0:
            setupsElem = nodes[0]
            allSetups = setupsElem.getElementsByTagName('setup')
            for setup in allSetups:
                self.setups[setup.getAttribute('name')] = Setup(setup)
            if self.setups[self.mainSetup] == None:
                raise BadScriptFormat('Main setup is missing!')

        #-----------------------------------
        #------- Steps definitions ---------
        #-----------------------------------
        stepsNode = builder.getElementsByTagName('steps')[0]
        self.steps = {}

        #------- STEP : Get Sources ---------
        nodes = stepsNode.getElementsByTagName('getSources')
        self.steps['getSources'] = (self.LoadSourceRepositories(nodes, 'getSources'), self.getSourcesStep)
        self.steps['tagSources'] = (self.LoadSourceRepositories(nodes, 'getSources'), self.tagSourcesStep)

        #------- STEP : Commit Sources ---------
        nodes = stepsNode.getElementsByTagName('commitSources')
        self.steps['commitSources'] = (self.LoadSourceRepositories(nodes, 'commitSources'), self.commitSourcesStep)

        #------- STEP : incProductVersion ---------
        nodes = stepsNode.getElementsByTagName('incProductVersion')
        incProductVersion = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'incProductVersion'
            allProducts = node.getElementsByTagName('projectName')
            products = []
            for product in allProducts:
                products.append(getText(product.childNodes))
            if incProductVersion.has_key(name):
                raise NameError, 'Name [%s] already used in incProductVersion steps' % name
            incProductVersion[name] = products
        if len(incProductVersion) == 0:
            incProductVersion['incProductVersion'] = self.mainProject
        self.steps['incProductVersion'] = (incProductVersion, self.incProductVersionStep)

        #------- STEP : updateProductVersion ---------
        nodes = stepsNode.getElementsByTagName('updateProductVersion')
        updateProductVersion = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'updateProductVersion'
            allProducts = node.getElementsByTagName('projectName')
            products = []
            for product in allProducts:
                products.append(getText(product.childNodes))
            if updateProductVersion.has_key(name):
                raise NameError, 'Name [%s] already used in updateProductVersion steps' % name
            updateProductVersion[name] = products
        self.steps['updateProductVersion'] = (updateProductVersion, self.updateProductVersionStep)

        #------- STEP : updateFileVersion ---------
        nodes = stepsNode.getElementsByTagName('updateFileVersion')
        updateFileVersion = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'updateFileVersion'
            allProducts = node.getElementsByTagName('projectName')
            fileVersions = []
            for product in allProducts:
                fileVersions.append(getText(product.childNodes))
            if updateFileVersion.has_key(name):
                raise NameError, 'Name [%s] already used in updateFileVersion steps' % name
            updateFileVersion[name] = fileVersions
        self.steps['updateFileVersion'] = (updateFileVersion, self.updateFileVersionStep)

        #------- STEP : build ---------
        nodes = stepsNode.getElementsByTagName('build')
        builds = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'build'
            buildDict = {}
            allEnvVars = node.getElementsByTagName('env')
            array = {}
            for env in allEnvVars:
                varName = env.getAttribute('name')
                varValue = os.path.expandvars(getText(env.childNodes))
                array[varName] = varValue
            buildDict['env'] = array

            allTouchFiles = node.getElementsByTagName('touchFile')
            array = []
            for fileName in allTouchFiles:
                array.append(os.path.expandvars(getText(fileName.childNodes)))
            buildDict['filesToTouch'] = array

            allDeleteFiles = node.getElementsByTagName('deleteFile')
            array = []
            for fileName in allDeleteFiles:
                array.append(os.path.expandvars(getText(fileName.childNodes)))
            buildDict['filesToDelete'] = array

            xmlAllBuilds = node.getElementsByTagName('projectName')
            array = []
            for xmlBuild in xmlAllBuilds:
                extraCompilOpt = xmlBuild.getAttribute('cl_add')
                buildDep = xmlBuild.getAttribute('dependencies') == '1'
                projectName = getText(xmlBuild.childNodes)
                if self.projects.has_key(projectName):
                    array.append((projectName, extraCompilOpt, buildDep))
                else:
                    raise BadScriptFormat('Unknown project found: \'%s\'' % projectName)
            buildDict['builds'] = array
            if builds.has_key(name):
                raise NameError, 'Name [%s] already used in build steps' % name
            builds[name] = buildDict
        self.steps['build'] = (builds, self.buildStep)

        #------- STEP : tests ---------
        nodes = stepsNode.getElementsByTagName('tests')
        tests = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'tests'
            allCustoms = node.getElementsByTagName('custom')
            testsSteps = []
            for custom in allCustoms:
                node = custom.getElementsByTagName('path')[0]
                path = os.path.expandvars( getText(node.childNodes) )
                node = custom.getElementsByTagName('cmd')[0]
                cmd = os.path.expandvars( getText(node.childNodes) )
                testsSteps.append( (path, cmd) )
            if tests.has_key(name):
                raise NameError, 'Name [%s] already used in tests steps' % name
            tests[name] = testsSteps
        self.steps['tests'] = (tests, self.externalStep)

        #------- STEP : protection ---------
        nodes = stepsNode.getElementsByTagName('protection')
        protection = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'protection'
            allCustoms = node.getElementsByTagName('custom')
            protectionSteps = []
            for custom in allCustoms:
                node = custom.getElementsByTagName('path')[0]
                path = os.path.expandvars( getText(node.childNodes) )
                node = custom.getElementsByTagName('cmd')[0]
                cmd = os.path.expandvars( getText(node.childNodes) )
                protectionSteps.append( (path, cmd) )
            if protection.has_key(name):
                raise NameError, 'Name [%s] already used in protection steps' % name
            protection[name] = protectionSteps
        self.steps['protection'] = (protection, self.externalStep)

        #------- STEP : makeInstall ---------
        nodes = stepsNode.getElementsByTagName('makeInstall')
        makeInstall = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'makeInstall'
            allSetupNames = node.getElementsByTagName('setupName')
            setupNames = []
            for setupName in allSetupNames:
                setupNames.append(getText(setupName.childNodes))
            if makeInstall.has_key(name):
                raise NameError, 'Name [%s] already used in makeInstall steps' % name
            makeInstall[name] = setupNames
        self.steps['makeInstall'] = (makeInstall, self.makeInstallStep)

        #------- STEP : publish ---------
        nodes = stepsNode.getElementsByTagName('publish')
        publish = {}
        for node in nodes:
            name = node.getAttribute('name')
            if name == '':
                name = 'publish'
            publishVars = {}
            for varNode in node.childNodes:
                publishVars[varNode.nodeName] = getText(varNode.childNodes)
            publish[name] = publishVars
        self.steps['publish'] = (publish, self.publishStep)

        #-----------------------------------
        #----------- Steps order -----------
        #-----------------------------------
        # Run steps
        runNode = builder.getElementsByTagName('run')[0]
        allSteps = runNode.getElementsByTagName('step')
        self.runSteps = {}
        self.runStepsNames = []
        for step in allSteps:
            type = step.getAttribute('type')
            stepName = getText(step.childNodes)
            if type == '':
                type = stepName
                fullName = stepName
            else:
                fullName = type + ': '+ stepName
            if type in _steps and self.steps.has_key(type):
                if self.steps[type][0].has_key(stepName) or type == stepName: #hack for old scripts
                    self.runStepsNames.append(fullName)
                    self.runSteps[fullName] = (type, stepName)
                else:
                    raise BadScriptFormat(u'\'%s\' step named \'%s\' not found!' % (type, stepName))
            else:
                raise BadScriptFormat(u'Unknown step found: \'%s\'' % type)



    def write(self, text):
        self.oldStdout.write(text)
        if type(text) is str:
            self.logFile.write(text.decode('latin').encode('utf8'))
        else:
            self.logFile.write(text.encode('utf8'))

    goodKeywords = {'cvsLog': None, 'makeAll': False, 'cvsTag': 'HEAD',
                    'cvsClean': False, 'incredibuild': False}

    def run(self, activeSteps, **keywords):
##        cvsLog = None, makeAll = False, cvsTag = 'HEAD'):
        for key in self.goodKeywords:
            if key not in keywords:
                keywords[key] = self.goodKeywords[key]
        for key in keywords:
            if key not in self.goodKeywords:
                raise KeyError, 'Key [%s] isn\'t supported on Script.run() function.'
            print key + ' = ' + str(keywords[key])
        prevCwd = os.getcwd()
        self.should_stop = False
        self.oldStdout = sys.stdout
        self.logFile = tempfile.NamedTemporaryFile('w+t')
        self.logFullPath = None
        sys.stdout = self
        try:
            for fullName in self.runStepsNames:
                if activeSteps.has_key(fullName) and activeSteps[fullName]:
                    type, stepName = self.runSteps[fullName]
                    if self.steps.has_key(type):
                        steps, function = self.steps[type]
                        function(steps, stepName, keywords)

            print u'\nBuild script finished with success !'
            if self.logFullPath != None:
                log = file(self.logFullPath, 'wt')
                log.write('test\n')
                self.logFile.seek(0)
                log.write(self.logFile.read())
                log.close()

        except BadReturnedValue:
            print u'\n*** A tool returned an error. Script aborted! ***'
        except BreakRequested:
            pass
        except :
            print u'\n*** Unknown exception. Script aborted! ***'
            excep = sys.exc_info()
            print sys.excepthook(excep[0], excep[1], excep[2])

        sys.stdout = self.oldStdout
        self.logFile.close()
        os.chdir(prevCwd)

    def getSourcesStep(self, steps, stepName, keywords):
        # #####################
        # Get lastest sources
        cvsGetList = steps[stepName]
        for cvsItem in cvsGetList:
            mycvs = eval('%s(cvsItem[0])' % cvsItem[2])
            if self.authentificationCallback:
                mycvs.SetAuthentificationCallback(self.authentificationCallback)
            for module in cvsItem[1]:
                self.curCmd = mycvs
                self.run_step(mycvs.update(module, tag = keywords['cvsTag'], clean = keywords['cvsClean']))

    def incProductVersionStep(self, steps, stepName, keywords):
        # #####################
        # Increment build number
        productList = steps[stepName]
        for product in productList:
            project = self.projects[product]
            visual = InstantiateVisualC(project.prjFile)
            version = visual.get_version(project.target)
            version = self.getVersion()
            if version == None:
                sys.stderr.write('Can\'t get version object!')
                raise BuildError, 'Can\'t get version object!'
            print u'Incrementing version for %s ...' % product
            version.inc_build()
            version.write_version()
            print u'%s new version value is %s' % (product, version.productVersion)


    def updateProductVersionStep(self, steps, stepName, keywords):
        # #####################
        # Updating products version
        version = self.getVersion()
        productList = steps[stepName]
        print u'Updating product version on: '
        for product in productList:
            print product, ',',
            project = self.projects[product]
            visual = InstantiateVisualC(project.prjFile)
            tmpVersion = visual.get_version(project.target)
            tmpVersion.productVersion = version.productVersion
            tmpVersion.write_version()
        print '.'

    def updateFileVersionStep(self, steps, stepName, keywords):
        # #####################
        # Updating products version
        version = self.getVersion()
        productList = steps[stepName]
        print u'Updating file version on: '
        for product in productList:
            print product, ',',
            project = self.projects[product]
            visual = InstantiateVisualC(project.prjFile)
            tmpVersion = visual.get_version(project.target)
            tmpVersion.fileVersion = version.productVersion
            tmpVersion.write_version()
        print '.'

    def buildStep(self, steps, stepName, keywords):
        # #####################
        # Build sources
        build = steps[stepName]
        print u'Starting compilation...'
        env = os.environ
        try:
            for name, value in build['env'].items():
                os.environ[name] = value
                print 'Set environment : %s = %s' % (name, value)
            for fileName in build['filesToTouch']:
                print 'Touching file %s...' % fileName
                os.utime(fileName, None)
            for fileName in build['filesToDelete']:
                try:
                    print 'Deleting file %s...' % fileName
                    os.remove(fileName)
                    print '  Ok'
                except:
                    print '  Error'
                    pass
            for build, extraCompilOpt, buildDep in build['builds']:
                project = self.projects[build]
                visual = InstantiateVisualC(project.prjFile)
                self.curCmd = visual
                if keywords['incredibuild']:
                    self.run_step(visual.incredibuild(project.target, project.config,
                                              isMakeAll = keywords['makeAll'], isNoDep = not buildDep,
                                              extraCompilOpt = extraCompilOpt))
                else:
                    self.run_step(visual.make(project.target, project.config,
                                              isMakeAll = keywords['makeAll'], isNoDep = not buildDep))
            print u'Compilation finished'
        except:
            os.environ = env
            raise

    def cleanProjects(self, projects, projectOnly = True):
        for name in projects:
            print 'Cleaning project', name, '...'
            project = self.projects[name]
            visual = InstantiateVisualC(project.prjFile)
            visual.clean(project.target, project.config, projectOnly)
        
    def externalStep(self, steps, stepName, keywords):
        # #####################
        # Run external program (such as protection, unittests, etc...
        external = steps[stepName]
        for step in external:
            hasp = Cmd()
            hasp.set_path(step[0])
            self.curCmd = hasp
            self.run_step(hasp(step[1]))

    def makeInstallStep(self, steps, stepName, keywords):
        # #####################
        # Make install exe
        install = steps[stepName]
        for setupName in install:
            inno = InnoSetup(self.setups[setupName].script)
            self.curCmd = inno
            self.run_step(inno.make_inst())

    def commitSourcesStep(self, steps, stepName, keywords):
        # #####################
        # Commit sources
        commitSources = steps[stepName]
        for cvsItem in commitSources:
            mycvs = eval('%s(cvsItem[0])' % cvsItem[2])
            if self.authentificationCallback:
                mycvs.SetAuthentificationCallback(self.authentificationCallback)
            for module in cvsItem[1]:
                self.curCmd = mycvs
                self.run_step(mycvs.commit(module, message = keywords['cvsLog']))

    def tagSourcesStep(self, steps, stepName, keywords):
        # #####################
        # Tag sources
        if stepName == 'tagSources' and not steps.has_key(stepName):
            stepName = 'getSources'
        tagSources = steps[stepName]
        version = self.getVersion()
        name = self.tagForVersion(version)
        for cvsItem in tagSources:
            mycvs = eval('%s(cvsItem[0])' % cvsItem[2])
            if self.authentificationCallback:
                mycvs.SetAuthentificationCallback(self.authentificationCallback)
            for module in cvsItem[1]:
                self.curCmd = mycvs
                self.run_step(mycvs.tag(module, name))

    def publishStep(self, steps, stepName, keywords):
        # #####################
        # Publish version
        publishStep = steps[stepName]

##        www_root = '\\\\nmgserver\\www'
##        prefix = 'DevSnapshot-'
##        productDir = 'RD/Products/%(productName)s'
##        htmlFullName = www_root + '/RD/%(productName)s_%(maj)d_%(min)d_Update.html'
##        symserver = '\\\\nmgserver\\symserver'
        www_root = publishStep['www_root']
        prefix = publishStep['prefix']
        productDir = publishStep['productDir']
        htmlFullName = publishStep['htmlFullName']
        symserver = publishStep['symserver']

        inno = InnoSetup(self.setups[self.mainSetup].script)
        version = self.getVersion()
        instFile = inno.get_output(version)
        dico = {'productName': self.name, 'maj' : version[0], 'min' : version[1],
                'rel' : version[2], 'build' : version[3]}
        instFileName = os.path.split(instFile)[1]
        publishedFile = prefix + instFileName
##        fileUrl = os.path.join('/RD/Products', self.name, publishedFile).replace('\\', '/')
        fileUrl = os.path.join('/', productDir % dico, publishedFile).replace('\\', '/')
##        fileFullPath = os.path.join(www_root, 'RD/Products', self.name, publishedFile)
        fileFullPath = os.path.join(www_root, productDir % dico, publishedFile)
        logUrl = os.path.splitext(fileUrl)[0] + '.log'
        self.logFullPath = os.path.splitext(fileFullPath)[0] + '.log'
        historyUrl = os.path.splitext(fileUrl)[0] + '-history.txt'
        historyPath = os.path.splitext(fileFullPath)[0] + '-history.txt'
        tmpPath = os.path.split(fileFullPath)[0]
        print 'tmpPath =', tmpPath
        if not os.path.exists(tmpPath):
            os.makedirs(tmpPath)
        copied = copy_file(instFile, fileFullPath)[1]
        if not copied:
            raise BuildError('Can''t copy file %s to %s' % (instFile, fileFullPath))
        historyFile = ''
        if publishStep and publishStep.has_key('historyFile'):
            print 'Copying file %s to %s' % (publishStep['historyFile'], historyPath)
            historyFile = publishStep['historyFile']
            copied = copy_file(publishStep['historyFile'], historyPath)[1]
            if not copied:
                raise BuildError('Can''t copy file %s to %s' % (publishStep['historyFile'], historyPath))
        PublishHTML(self.name, fileUrl, version_format(version),
                    self.htmlTemplate, htmlFullName  % dico, logUrl, historyUrl )
        strVersion = '%(maj)d_%(min)d_%(rel)d_%(build)d' %  dico
        pdbFileName = os.path.join(www_root, productDir % dico, self.name + '_' + strVersion + '.zip')
        if os.path.exists(pdbFileName):
            os.remove(pdbFileName)
        print 'Creating archive for PDB files:', pdbFileName
        pdbZipFile = ZipFile(pdbFileName, 'w', ZIP_DEFLATED)
        try:
            for projectName in self.projects:
                project = self.projects[projectName]
                if len(project.pdb):
                    pdbPath, pdbLeafName = os.path.split(project.pdb)
                    print 'Adding', project.pdb, 'as', pdbLeafName, 'into archive...'
                    pdbZipFile.write(str(project.pdb), str(pdbLeafName))
                    if pdbLeafName.lower().endswith('.pdb'):
                        symstore = Cmd()
                        symstore.set_path(pdbPath)
                        print 'Adding', project.pdb, 'into symbols database...'
                        self.curCmd = symstore
                        self.run_step(symstore("symstore add /f \"" + pdbLeafName
                                      + "\" /s " + symserver + " /t \"" + self.name
                                      + "\" /v \"" + version_format(version) + "\""))
            # TODO: Add binaries too
        except:
            pdbZipFile.close()
            raise


    def getVersion(self):
        project = self.projects[self.mainProject]
        visual = InstantiateVisualC(project.prjFile)
        version = visual.get_version(project.target)
        return version

    def tagForVersion(self, version):
        strVersion = '%(maj)d_%(min)d_%(rel)d_%(build)d'\
                   %  {'maj' : version[0], 'min' : version[1], \
                   'rel' : version[2], 'build' : version[3]}
        return self.name.upper() + '_' + strVersion

    def run_step(self, cmd):
        self.curCmd = None
        if self.should_stop:
            print 'Script interuption requested by user!'
            self.should_stop = False
            raise BreakRequested
        elif cmd != 0:
            sys.stderr.write('Bad returned value: %d' % cmd)
            raise BadReturnedValue(cmd)

    def kill(self):
        print 'STOP!!!'
        self.should_stop = True
        if self.curCmd != None:
            print 'STOP!!!'
            self.curCmd.stop()

    def getTagList(self):
        t = time.time()
        tagList = []
        branchList = []
        tagSources = self.steps['tagSources']
        print tagSources[0]
        for cvsItem in tagSources[0]['getSources']:
            print cvsItem
            mycvs = eval('%s(cvsItem[0])' % cvsItem[2])
            if self.authentificationCallback:
                mycvs.SetAuthentificationCallback(self.authentificationCallback)
            for module in cvsItem[1]:
                tagList.extend(mycvs.get_tag_list(module, root = 'tags'))
                branchList.extend(mycvs.get_tag_list(module, root = 'branches'))
        print 'TAGs list uploaded in %f seconds' % (time.time() - t)
        return tagList, branchList
                
        #project = self.projects[self.mainProject]
        #for cvsItem in self.steps['getSources'][0].values()[0]:
            #if project.prjFile.find(cvsItem[0]) >= 0:
                #file = project.prjFile.replace(cvsItem[0], './')
                #print cvsItem[0]
                #print file
                #mycvs = eval('%s(cvsItem[0])' % cvsItem[2])
                #if self.authentificationCallback:
                    #mycvs.SetAuthentificationCallback(self.authentificationCallback)
                ##mycvs.set_output_callback(self.tagLogParser)
                ##mycvs('log -h ' + ' "' + file + '"')
        #print 'TAGs list uploaded in %f seconds' % (time.time() - t)
        #return self.tagList

    def getCurrentTagName(self):
        #try:
            #project = self.projects[self.mainProject]
        #except KeyError:
            #project = None
        #if project:
            #tagFileName = project.prjFile
            #if os.path.exists(project.prjFile):
        tagSources = self.steps['tagSources']
        print tagSources[0]
        cvsItem = tagSources[0]['getSources'][0]
        print cvsItem
        mycvs = eval('%s(cvsItem[0])' % cvsItem[2])
        if self.authentificationCallback:
            mycvs.SetAuthentificationCallback(self.authentificationCallback)
        type, name = mycvs.get_current_tag_name(cvsItem[1][0]) # Only first module
        return name

        #try:
            #project = self.projects[self.mainProject]
        #except KeyError:
            #project = None
        #if project:
            #tagFileName = os.path.join(os.path.split(project.prjFile)[0], 'CVS', 'Tag')
            #if os.path.exists(tagFileName):
                #tag = open(tagFileName, 'rt').readline()[1:].strip()
                #return tag
        #return None

    #def tagLogParser(self, lines):
        #for line in lines.splitlines():
            #if len(line) > 0 and line[0] == '\t':
                #tag, version = line.strip().split(': ', 1)
###                if len(version.strip().split('.')) > 2 and version.strip() != '1.1.1.1':
                #if self.tagList.count(tag) == 0:
                    #self.tagList.append(tag)
                    #print tag
            #elif len(line) > 8 and line[:8] == 'cvs log:':
                #sys.stderr.write(line + '\n')

    def makeBranch(self, tagName, branchName):
        t = time.time()
        self.tagList = []
        cvsGetList = self.steps['getSources'][0].values()[0]
        hasError = False
        try:
            for cvsItem in cvsGetList:
                mycvs = eval('%s(cvsItem[0])' % cvsItem[2])
                if self.authentificationCallback:
                    mycvs.SetAuthentificationCallback(self.authentificationCallback)
                moduleList = ''
                for module in cvsItem[1]:
                    self.run_step(mycvs.make_branch(module, branchName, tagName))
                #print '*** cvs tag -b -r ' + tagName + ' ' + branchName + ' ' + moduleList
            print 'Branch ''%s'' created in %f seconds' % (branchName, (time.time() - t))
        except BadReturnedValue:
            print u'\n*** A tool returned an error. Script aborted! ***'
        except BreakRequested:
            pass
        except :
            print u'\n*** Unknown exception. Script aborted! ***'
            excep = sys.exc_info()
            print sys.excepthook(excep[0], excep[1], excep[2])

if __name__ == '__main__':
    script = Script('D:/Develop/Python/TestXML/BuildScript.xml')
    print str(script.projects['AdminAgent'].prjFile)
    print script.cvsGetList
    print script.cvsCommitList
    print script.builds
    print script.setupNames
    print script.runSteps

    steps = {}
    for stepName in _steps:
        steps[stepName] = False
    for stepName in script.runSteps:
        steps[stepName] = False        # Should be true here
#    steps['getSources'] = False
#    steps['incProductVersion'] = False
#    steps['build'] = False
#    steps['makeInstall'] = False
#    steps['commitSources'] = False
    steps['publish'] = True
    script.run(steps, cvsLog = 'Test du script')

    print 'End...'
