import os
import re

from iwCmd import *

class InnoSetup(Cmd):
    def __init__(self, script, compiler_path = None):
        """Creates the InnoSetup wrapper class.

           If the compiler path is given to the constructor, it overides the
           environment variable, else you have to define 'INNO_SETUP' variable
           to fully calified path to the compiler (including the compiler itself)."""
        Cmd.__init__(self)
        if compiler_path != None:
            self.__cmd = compiler_path
        else:
            self.__cmd = os.environ['INNO_SETUP']
        self.__script = script

    def __call__(self, script, version = None, compiler_path = None):
        self.__init__(script, comipler_path)
        return self.make_inst()

    def make_inst(self, version = None):
        """Execute an 'update' command."""
        if version != None:
            self.set_version(version)
        (path, script_name) = os.path.split(self.__script)
        Cmd.set_path(self, path)
        cmd = [self.__cmd, '"' + self.__script + '"']
        return Cmd.run_cmd(self, cmd)

    def set_version(self, version):
        fd = file(self.__script, 'rt')
        lines = fd.readlines()
        fd.close()
        (setupLine, index) = find_line('[Setup]', lines)
        # get app name
        (line, index) = find_line('AppName=', lines, setupLine)
        index2 = lines[line].index('\n')
        appName = lines[line][8:index2]
        # compute new values
        appVersion = '%(maj)d.%(min)d'% {'maj': version[0], 'min': version[1]}
        appFullVersion = appVersion + '.%(rel)d.%(build)d'% {'rel': version[2], 'build': version[3]}
        appFriendlyVersion = appVersion + '.%(rel)d build %(build)d'% {'rel': version[2], 'build': version[3]}
        setupName = '%(name)s-%(maj)d.%(min)d.%(rel)d.%(build)d-setup' \
                        % {'name': appName, 'maj': version[0],  'min': version[1],  'rel': version[2], 'build': version[3]}
        # store new values
        (line, index) = find_line('AppVerName=', lines, setupLine)
        lines[line] = 'AppVerName=' + appName + ' v' + appFriendlyVersion + '\n'
        (line, index) = find_line('AppVersion=', lines, setupLine)
        lines[line] = 'AppVersion=' + appVersion + '\n'
        (line, index) = find_line('OutputBaseFilename=', lines, setupLine)
        lines[line] = 'OutputBaseFilename=' + setupName + '\n'
        try:
            (line, index) = find_line('VersionInfoVersion=', lines, setupLine)
            lines[line] = 'VersionInfoVersion=' + appFullVersion + '\n'
            (line, index) = find_line('VersionInfoTextVersion=', lines, setupLine)
            lines[line] = 'VersionInfoTextVersion=' + appName + ' v' + appFriendlyVersion + '\n'
        except ValueError:
            pass    # No VersionInfo section
        fd = file(self.__script, 'wt')
        fd.writelines(lines)
        fd.close()

    def get_output(self, version = None):
        fd = open(self.__script, 'r+t')
        lines = fd.readlines()
        fd.close()
        defMap = {}
        defExpr = re.compile('[ \t]*#define[ \t]+([^ \t]+)[ \t]+"([^"]+)"')
        for line in lines:
            define = defExpr.match( line )
            if define:
                defMap[define.group(1)] = define.group(2)
                
        (setupLine, index) = find_line('[Setup]', lines)
        (line, index) = find_line('OutputBaseFilename=', lines, setupLine)
        str = lines[line]
        index = str.index('=') +1
        index2 = str.index('\n')
        fileName = str[index:index2] + '.exe'
        try:
            (line, index) = find_line('OutputDir=', lines, setupLine)
            str = lines[line]
            index = str.index('=') +1
            index2 = str.index('\n')
            outputDir = str[index:index2]
        except:
            outputDir = 'Output'
        try:
            (line, index) = find_line('SourceDir=', lines, setupLine)
            str = lines[line]
            index = str.index('=') +1
            index2 = str.index('\n')
            sourceDir = str[index:index2]
        except:
            sourceDir = '.'
        (path, script_name) = os.path.split(self.__script)
        fullname = os.path.join(path, sourceDir, outputDir, fileName)
        
        if fullname.find('{#AppName}') >= 0:
            fullname = fullname.replace('{#AppName}', defMap['AppName'])
        if version:
            appVersion = '%(maj)d.%(min)d.%(rel)d.%(build)d' \
                            % {'maj': version[0],  'min': version[1],  'rel': version[2], 'build': version[3]}
            if fullname.find('{#AppVersion}') >= 0:
                fullname = fullname.replace('{#AppVersion}', appVersion)
            else:
                sys.stderr.write('InnoSetup script is not \'ScripManager compliant\'!\n')
                sys.stderr.write('The \'OutputBaseFilename\' value should use the macro \'{#AppVersion}\'!\n')

        return fullname



def find_line(str, lines, starting = 0):
    """Find un a string array the string with specified pattern.
       Returns a Turple with the line index (begining at 0) and the position
       of the wanted string. If not found, returns (None, None)."""
    i = starting
    for line in lines[starting:]:
        index = line.find(str)
        if index >= 0:
            return (i, index)
        i += 1
    raise ValueError, str
