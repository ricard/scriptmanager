import os
import thread
import sys

BadReturnValue = 'BadReturnValue'
BreakRequested = 'BreakRequested'


class ScriptBase:
    """Not used. Old base class for scripts."""
    def __init__(self):
        self.should_stop = False
        self.curCmd = None

    def run_step(self, cmd):
        self.curCmd = cmd
        try:
            ret = cmd
        except:
            self.curCmd = None
        if ret != 0:
            sys.stderr.write('Bad returned value: %d' % ret)
            raise BadReturnValue
        elif self.should_stop:
            print 'Script interuption requested by user!'
            self.should_stop = False
            raise BreakRequested

    def kill(self):
        print 'STOP!!!\n'
        self.should_stop = True
        if self.curCmd != None:
            self.curCmd.stop()

    def run(self, steps):
        should_stop = False

    def BatchBuild(self, visual, projects, config = 'Win32 Release', version = None, isMakeAll = False, isNoDep = False):
        for project in projects:
            if version != None:
                tmpVersion = visual.get_version(project)
                tmpVersion.productVersion = version.productVersion
                tmpVersion.write_version()
            self.run_step(visual.make([project + ' - ' + config], False, True))

    def GetVersion(self):
        raise

    def IncBuildNumber(self):
        version = self.GetVersion()

        # #####################
        # Increment build number
        if steps['_inc_version']:
            if version == None:
                print 'Can\'t get version object !'
                raise BadReturnValue
            print 'Incrementing version...'
            version.inc_build()
            version.write_version()
            print 'New version value is', version.productVersion

def FileCopy(src, dst):
    """Make a binary copy of a file."""
    (path, name) = os.path.split(dst)
    if name == '':
        (path, name) = os.path.split(src)
        dst = dst + name
    print 'Copying file "' + src + '" to "' + dst + '"...'
    file(dst, 'wb').write(file(src, 'rb').read())
    return 0

def version_format(version):
    str = '%(maj)d.%(min)d.%(rel)d build %(build)d'\
            %  {'maj' : version[0], 'min' : version[1], 'rel' : version[2], 'build' : version[3]}
    return str
