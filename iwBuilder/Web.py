import time

def PublishHTML(name, target, version, htmlTemplate, htmlTarget, logUrl = '', historyUrl = ''):
    """Plugin allowing to publish every new release of a product to a web site.

       It looks into a htmlFile template and replace 4 TAGs :
           - __PRODUCT_NAME__ = Name of the product
           - __PRODUCT_VERSION__ = Full version information
           - __SETUP_LINK__ = Path on the server to the setup package
           - __DATE__ = Build date"""
    print 'Publishing HTML file', htmlTarget, 'for product', name, version
    fd = file(htmlTemplate, 'rt')
    html = fd.read()
    html = html.replace('__PRODUCT_NAME__', str(name))
    html = html.replace('__PRODUCT_VERSION__', str(version))
    html = html.replace('__SETUP_LINK__', str(target))
    html = html.replace('__DATE__', str(time.strftime('%d/%m/%y a %H:%M')))
    html = html.replace('__LOG__', str(logUrl))
    html = html.replace('__HISTORY__', str(historyUrl))
    fd = file(htmlTarget, 'wt')
    fd.write(html)
    fd.close()
    return 0
