import os
import types
from iwCmd import *

class cvs(Cmd):
    def __init__(self, path, cvscmd = 'cvs'):
        """Creates the cvs wrapper class.

           The optional 'cvscmd' parameter allows to specify the full absolute
           or relative path to the cvs executable.
           If ommited, the application will try to find 'cvs.exe' into the path
           environment."""
        Cmd.__init__(self)
        Cmd.set_path(self, path)
        self.__cmd = cvscmd
#        try:
#            homedrive = os.environ['HOMEDRIVE']
#            homepath = os.environ['HOMEPATH']
#        except:
#            print 'Environment variable HOMEDRIVE not defined.'
#            raise
#        self.__home = homedrive + homepath
#        try:
#            self.__cvsroot = os.environ['CVSROOT']
#        except:
#            self.__cvsroot = ''
        self.__cvsroot = ''

    def update(self, modules, tag = None, clean = False):
        """Execute an 'update' command."""
        cmd = [self.__cmd]
        if len(self.__cvsroot) > 0:
            cmd += ['-d', self.__cvsroot]
        cmd += ['-z9',  'update', '-d', '-R', '-P']
        if clean:
            cmd += ['-C']
        if tag == 'HEAD' or tag == None:
            cmd += ['-A']
        else:
            cmd += ['-r ' + tag]
        if isinstance(modules, types.StringTypes):
            cmd += ['"' + modules + '"']
        else:
            cmd += map(lambda x: '"' + x + '"' , modules)
        return Cmd.run_cmd(self, cmd)

    def commit(self, modules, message = None):
        """Execute an 'commit' command."""
        cmd = [self.__cmd]
        if len(self.__cvsroot) > 0:
            cmd += ['-d', self.__cvsroot]
        cmd += ['-z9',  'commit', '-R']
        if message != None:
            newMsg = ''
            for c in message:
                if c == '\'':
                    newMsg += '\\\''
                else:
                    newMsg += c
            cmd += ['-m', '"' + newMsg + '"']
        else:
            cmd += ['-m', '"Autobuild commit"']
        if isinstance(modules, types.StringTypes):
            cmd += ['"' + modules + '"']
        else:
            cmd += map(lambda x: '"' + x + '"' , modules)
        return Cmd.run_cmd(self, cmd)
        
    def tag(self, modules, tagName, replace = True):
        cmd = [self.__cmd]
        if len(self.__cvsroot) > 0:
            cmd += ['-d', self.__cvsroot]
        cmd += ['-z9',  'tag', '-R', '-F', tagName]
        if isinstance(modules, types.StringTypes):
            cmd += ['"' + modules + '"']
        else:
            cmd += map(lambda x: '"' + x + '"' , modules)
        return Cmd.run_cmd(self, cmd)
    
    def __call__(self, commandLine):
        cmd = [self.__cmd]
        if len(self.__cvsroot) > 0:
            cmd += ['-d', self.__cvsroot]
        cmd += ['-z9']
        for item in commandLine.split(' '):
            cmd += [item]
        return Cmd.run_cmd(self, cmd)
        

    def set_cvsroot(self, cvsroot):
        self.__cvsroot = cvsroot;

    def load_cvsroot(self, path):
        tmpStr = file(path + '/CVS/root', 'rt').readline()
        index = 0
        for c in tmpStr:
            if ord(c) < 32:  # special char like EOL
                break
            index = index + 1
        cvsroot = tmpStr[:index]
        self.set_cvsroot(cvsroot)
