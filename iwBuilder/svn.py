import os
import types
from iwCmd import *
import pysvn
import re

wc_status_kind_map = {
    pysvn.wc_status_kind.added: 'A',
    pysvn.wc_status_kind.conflicted: 'C',
    pysvn.wc_status_kind.deleted: 'D',
    pysvn.wc_status_kind.external: 'X',
    pysvn.wc_status_kind.ignored: 'I',
    pysvn.wc_status_kind.incomplete: '!',
    pysvn.wc_status_kind.missing: '!',
    pysvn.wc_status_kind.merged: 'G',
    pysvn.wc_status_kind.modified: 'M',
    pysvn.wc_status_kind.none: ' ',
    pysvn.wc_status_kind.normal: ' ',
    pysvn.wc_status_kind.obstructed: '~',
    pysvn.wc_status_kind.replaced: 'R',
    pysvn.wc_status_kind.unversioned: '?',
}

wc_notify_action_map = {
    pysvn.wc_notify_action.add: 'A',
    pysvn.wc_notify_action.commit_added: 'A',
    pysvn.wc_notify_action.commit_deleted: 'D',
    pysvn.wc_notify_action.commit_modified: 'M',
    pysvn.wc_notify_action.commit_postfix_txdelta: None,
    pysvn.wc_notify_action.commit_replaced: 'R',
    pysvn.wc_notify_action.copy: 'c',
    pysvn.wc_notify_action.delete: 'D',
    pysvn.wc_notify_action.failed_revert: 'F',
    pysvn.wc_notify_action.resolved: 'R',
    pysvn.wc_notify_action.restore: 'R',
    pysvn.wc_notify_action.revert: 'R',
    pysvn.wc_notify_action.skip: 'skip',
    pysvn.wc_notify_action.status_completed: None,
    pysvn.wc_notify_action.status_external: 'X',
    pysvn.wc_notify_action.update_add: 'A',
    pysvn.wc_notify_action.update_completed: None,
    pysvn.wc_notify_action.update_delete: 'D',
    pysvn.wc_notify_action.update_external: 'X',
    pysvn.wc_notify_action.update_update: 'U',
    pysvn.wc_notify_action.annotate_revision: 'A',
}

if hasattr( pysvn.wc_notify_action, 'locked' ):
    wc_notify_action_map[ pysvn.wc_notify_action.locked ] = 'locked'
    wc_notify_action_map[ pysvn.wc_notify_action.unlocked ] = 'unlocked'
    wc_notify_action_map[ pysvn.wc_notify_action.failed_lock ] = 'failed_lock'
    wc_notify_action_map[ pysvn.wc_notify_action.failed_unlock ] = 'failed_unlock'

class svn:
    def __init__(self, path):
        """Creates the svn wrapper class."""
        self.__path = path
        self.authCallback = None
        self.shouldStop = False
        self.revision_update_complete = None
        self.log_message = ''

    def SetAuthentificationCallback(self, cb):
        self.authCallback = cb
        
    def output_callback(self, txt):
        sys.stdout.write(txt.decode('cp850'))
            
    def callback_cancel( self ):
        return self.shouldStop
    
    def get_log_message(self):
        return True, self.log_message

    def stop(self):
        self.shouldStop = True

    def callback_notify( self, arg_dict ):
        if arg_dict['path'] != '':
            if arg_dict['action'] == pysvn.wc_notify_action.update_completed:
                self.revision_update_complete = arg_dict['revision']
                print 'Updated', arg_dict['path'], 'to revision', self.revision_update_complete.number
            elif arg_dict['action'] == pysvn.wc_notify_action.update_external:
                msg = '>>> External %s' % (arg_dict['path'],)
                print msg
            elif wc_notify_action_map[ arg_dict['action'] ] is not None:
                msg = '%s %s' % (wc_notify_action_map[ arg_dict['action'] ], arg_dict['path'])
                print msg

    def update(self, module, tag = None, clean = False):
        """Execute an 'update' command."""
        assert(isinstance(module, types.StringTypes))
        oldwd = os.getcwd()
        self.revision_update_complete = None
        try:
            os.chdir(self.__path)
            client = pysvn.Client(os.path.expanduser('~/ScriptManager.svn'))
            client.exception_style = 1
            client.callback_notify = self.callback_notify
            client.callback_cancel = self.callback_cancel
            if self.authCallback:
                client.callback_get_login = self.authCallback
            if clean:
                print '*** Undoing all changes in module \'%s\'...' % module
                client.revert(module, recurse = True)
            actionIsSwitch = True
            if tag == 'HEAD' or tag == 'trunk' or tag == None:
                url = client.info(module).url
                if url.find('/tags/') > 0:
                    url = os.path.split(url.replace('/tags/', '/trunk/'))[0];
                elif url.find('/releases/') > 0:
                    url = os.path.split(url.replace('/releases/', '/trunk/'))[0];
                elif url.find('/branches/') > 0:
                    url = os.path.split(url.replace('/branches/', '/trunk/'))[0];
                else:
                    actionIsSwitch = False
            else:
                url = client.info('./' + module).url
                if url.find('/branches/') > 0:
                    if not url.endswith(tag):
                        url = os.path.split(url)[0] + '/' + tag;
                    else:
                        actionIsSwitch = False
                        client.update(module)
                elif url.find('/releases/') > 0:
                    url = os.path.split(url.replace('/releases/', '/branches/'))[0] + '/' + tag;
                elif url.find('/tags/') > 0:
                    url = os.path.split(url.replace('/tags/', '/branches/'))[0] + '/' + tag;
                elif url.find('/trunk/') > 0 or url.endswith('/trunk'): # special case
                    url = url.replace('/trunk', '/branches') + '/' + tag;
                else:
                    raise Exception('Error parsing the subvertion URL for "%s". Url = %s' % (module, url))
            try:
                client.ls(url, recurse = False)
            except:
                sys.stderr.write('URL \'%s\' not found. Are you sure that the branch already exists ?\n' % url)
                return -1;
            if actionIsSwitch:
                print '*** Switching module \'%s\' to url \'%s\'...' % (module, url)
                client.switch(module, url, recurse = True)
            else:
                print '*** Updating module \'%s\' on url \'%s\'...' % (module, url)
                client.update(module)
        finally:
            os.chdir(oldwd)
        return 0

    def commit(self, module, message = ''):
        """Execute an 'commit' command."""
        assert(isinstance(module, types.StringTypes))
        oldwd = os.getcwd()
        try:
            os.chdir(self.__path)
            client = pysvn.Client(os.path.expanduser('~/ScriptManager.svn'))
            client.exception_style = 1
            client.callback_notify = self.callback_notify
            client.callback_cancel = self.callback_cancel
            if self.authCallback:
                client.callback_get_login = self.authCallback
            if message == None or message == '':
                message = 'Autobuild commit'
            print '*** Commiting modified files in module \'%s\' to Subversion...' % module
            client.checkin(module, message)
        finally:
            os.chdir(oldwd)
        return 0
        
    def tag(self, module, tagName):
        assert(isinstance(module, types.StringTypes))
        oldwd = os.getcwd()
        try:
            os.chdir(self.__path)
            client = pysvn.Client(os.path.expanduser('~/ScriptManager.svn'))
            client.exception_style = 1
            client.callback_notify = self.callback_notify
            client.callback_cancel = self.callback_cancel
            if self.authCallback:
                client.callback_get_login = self.authCallback
            client.callback_get_log_message = self.get_log_message
            
            url = self.MakeUrlFor(client, module, 'tags', tagName)
            
            try:
                client.ls(url, recurse = False)
                sys.stderr.write('URL \'%s\' already exists. TAG %s was previously created.\n' % (url, tagName))
                sys.stderr.write('It seems that this version was already produced, please increment version number.\n')
                return -1;
            except:
                pass   # url not found = OK
            self.log_message = "ScriptManager: creating tag " + tagName
            print '*** TAG: Copying module \'%s\' to url \'%s\'...' % (module, url)
            # create all subdirs
            self.CreateAllDirs(client, os.path.split(url)[0])
            client.copy(module, url)

        finally:
            os.chdir(oldwd)
        return 0
    
    def make_branch(self, module, branchName, tagName):
        assert(isinstance(module, types.StringTypes))
        oldwd = os.getcwd()
        try:
            os.chdir(self.__path)
            client = pysvn.Client(os.path.expanduser('~/ScriptManager.svn'))
            client.exception_style = 1
            client.callback_notify = self.callback_notify
            client.callback_cancel = self.callback_cancel
            if self.authCallback:
                client.callback_get_login = self.authCallback
            client.callback_get_log_message = self.get_log_message
            
            srcUrl = self.MakeUrlFor(client, module, 'tags', tagName)
            dstUrl = self.MakeUrlFor(client, module, 'branches', branchName)
            
            try:
                client.ls(dstUrl, recurse = False)
                sys.stderr.write('URL \'%s\' already exists. BRANCH %s was previously created.\n' % (dstUrl, tagName))
                sys.stderr.write('Please choose another name for your branch.\n')
                return -1;
            except:
                pass   # url not found = OK

            self.log_message = "ScriptManager: creating branch " + branchName
            print '*** BRANCH: Copying url \'%s\' to url \'%s\'...' % (srcUrl, dstUrl)
            # create all subdirs
            self.CreateAllDirs(client, os.path.split(dstUrl)[0])
            client.copy(srcUrl, dstUrl)

        finally:
            os.chdir(oldwd)
        return 0

    def get_tag_list(self, module, root = 'tags'):
        assert(isinstance(module, types.StringTypes))
        oldwd = os.getcwd()
        try:
            os.chdir(self.__path)
            client = pysvn.Client(os.path.expanduser('~/ScriptManager.svn'))
            client.exception_style = 1
            client.callback_notify = self.callback_notify
            client.callback_cancel = self.callback_cancel
            if self.authCallback:
                client.callback_get_login = self.authCallback
            
            url = self.MakeUrlFor(client, module, root, '')

            print '*** Get listing from url \'%s\'...' % url
            tags = []
            try:
                entries_list = client.ls(url, recurse = False)
                for entry in entries_list:
                    if entry['kind'] == pysvn.node_kind.dir:
                        tags.append(os.path.split(entry['name'])[1])
            except:
                pass

        finally:
            os.chdir(oldwd)
        return tags

    def get_current_tag_name(self, module):
        assert(isinstance(module, types.StringTypes))
        oldwd = os.getcwd()
        try:
            os.chdir(self.__path)
            client = pysvn.Client(os.path.expanduser('~/ScriptManager.svn'))
            client.exception_style = 1
            client.callback_notify = self.callback_notify
            client.callback_cancel = self.callback_cancel
            if self.authCallback:
                client.callback_get_login = self.authCallback
            
            print '*** Getting tag information from module \'%s\'...' % module
            url = client.info(module).url
            
            trunkRe = re.compile(r'(.+[/\\])(trunk)((?:[/\\].+)?)')
            othersRe = re.compile(r'(.+[/\\])(tags|releases|branches)((?:[/\\].+)?)[/\\]([^/\\]+)')

            trunkM = trunkRe.match(url)
            if trunkM:
                return 'trunk', 'trunk'
            othersM = othersRe.match(url)
            if othersM:
                return othersM.group(2), othersM.group(4)
    
        finally:
            os.chdir(oldwd)
        return None, None
    
    def MakeUrlFor(self, client, module, rootFolder, name):
        """Return the url string for the specified folder ('trunk', 'tags', 'releases' or 'branches')
           with the specified tag name"""
            
        url = client.info(module).url
        
        trunkRe = re.compile(r'(.+[/\\])(trunk)((?:[/\\].+)?)')
        othersRe = re.compile(r'(.+[/\\])(tags|releases|branches)((?:[/\\].+)?)[/\\]([^/\\]+)')
        trunkM = trunkRe.match(url)
        othersM = othersRe.match(url)
        if trunkM:
            baseUrl = trunkM.group(1)
            moduleUrl = trunkM.group(3)
        elif othersM:
            baseUrl = othersM.group(1)
            moduleUrl = othersM.group(3)
        else:
            raise Exception('Error parsing the subvertion URL for "%s". Url = %s' % (module, url))
        return baseUrl + rootFolder + moduleUrl + '/' + name
        
        
    def CreateAllDirs(self, client, path):
        try:
            client.ls(path, recurse = False)
        except:
            def create_dir_recurs(client, path):
                basePath = os.path.split(path)[0]
                if path == '' or path == basePath:
                    raise NameError('CreateAllDirs: Path not found')
                try:
                    client.ls(basePath, recurse = False)
                except:
                    create_dir_recurs(client, basePath)
                print 'Making directory \'%s\'...' % path
                client.mkdir(path, 'ScriptManager: creating directories')
            create_dir_recurs(client, path)
