#Boa:Frame:MainFrame
# pylint: disable-msg=W0611

import sys
from wxPython.wx import *
from wxPython.stc import *
from wxPython.lib.anchors import LayoutAnchors
from wxUtils import *
from imp import *
from threading import *
import os
import traceback
from iwBuilder import Script
import ProjectDialog
import OptionsDialog
from Crypto.Cipher import Blowfish

_scripts = {
    #('Agent', '../AdminAgent/build/BuildAgent.xml'),
    '** ScriptManager **': 'MajScript.xml',
    'I-MX': '../../IW/imx/build/BuildIMX.xml',
    'OSDA Manager': '../../SorbetDAmour/OSDAManager/Build/Build OSDA Manager.xml',
    'OSDA Store': '../../SorbetDAmour/OSDA Store/Build/Build OSDA Store.xml',
    'SQLiteEdit': '../../CSharp/SQLiteEdit/Build/Build SQLiteEdit.xml',
    }

def create(parent):
    return MainFrame(parent)

class ErrHandler:
    def __init__(self, logger):
        self.logger = logger

    def write(self, text):
        pos = self.logger.GetInsertionPoint()
        self.logger.write(text)
        endPos = self.logger.GetInsertionPoint()
        textAttr = self.logger.GetDefaultStyle()
        font = textAttr.GetFont()
        font.SetWeight(wx.BOLD)
        attr = wx.TextAttr(colText = wx.RED, font = font)
        self.logger.SetStyle(pos, endPos, attr)

[wxID_MAINFRAME, wxID_MAINFRAMEAPPLYVERSION, wxID_MAINFRAMEBACKGROUND,
 wxID_MAINFRAMEBRANCHLABEL, wxID_MAINFRAMECLEANBUTTON,
 wxID_MAINFRAMECLEANCOPY, wxID_MAINFRAMECVSBRANCH,
 wxID_MAINFRAMECVSUPDATEBUTTON, wxID_MAINFRAMEINCMAJORBUTTON,
 wxID_MAINFRAMEINCMINORBUTTON, wxID_MAINFRAMEINCREDIBUILD,
 wxID_MAINFRAMEINCRELEASEBUTTON, wxID_MAINFRAMELOADSCRIPTBUTTON,
 wxID_MAINFRAMELOGGER, wxID_MAINFRAMEMAKEALLCHECKBOX,
 wxID_MAINFRAMEMESSAGEHISTORY, wxID_MAINFRAMEMESSAGETEXTCTRL,
 wxID_MAINFRAMENEWBRANCHBUTTON, wxID_MAINFRAMEOPTIONS,
 wxID_MAINFRAMEQUITBUTTON, wxID_MAINFRAMEREFRESHTAGLISTBUTTON,
 wxID_MAINFRAMERUNSCRIPTBUTTON, wxID_MAINFRAMESAVELOG,
 wxID_MAINFRAMESCRIPTPROGRESS, wxID_MAINFRAMESCRIPTSCOMBO,
 wxID_MAINFRAMESPINCTRL1, wxID_MAINFRAMESPINCTRL2, wxID_MAINFRAMESPINCTRL3,
 wxID_MAINFRAMESPINCTRL4, wxID_MAINFRAMESTATICTEXTVERSION,
 wxID_MAINFRAMESTEPSLIST, wxID_MAINFRAMESTOPSCRIPTBUTTON,
] = [wx.NewId() for _init_ctrls in range(32)]

class MainFrame(wxFrame):
    __steps = {}

    def _init_coll_bottomSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.saveLog, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0, proportion=1)
        parent.AddWindow(self.Options, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0, proportion=1)
        parent.AddWindow(self.quitButton, 0, border=0, flag=0)

    def _init_coll_topSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.topLeftSizer, 1, border=10, flag=wx.ALL)
        parent.AddWindow(self.stepsList, 1, border=10, flag=wx.ALL | wx.GROW)
        parent.AddSizer(self.cmdSizer, 0, border=10, flag=wx.ALL | wx.GROW)

    def _init_coll_incVersionSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.incMajorButton, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0, proportion=1)
        parent.AddWindow(self.incMinorButton, 0, border=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0, proportion=1)
        parent.AddWindow(self.incReleaseButton, 0, border=0)

    def _init_coll_frameSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.topSizer, 0, border=0, flag=wx.GROW)
        parent.AddWindow(self.messageHistory, 0, border=10,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.GROW)
        parent.AddSpacer(wx.Size(8, 2), border=0, flag=0)
        parent.AddWindow(self.messageTextCtrl, 0, border=10,
              flag=wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.GROW)
        parent.AddWindow(self.scriptProgress, 0, border=10,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.GROW)
        parent.AddWindow(self.logger, 1, border=10, flag=wx.ALL | wx.GROW)
        parent.AddSizer(self.bottomSizer, 0, border=10, flag=wx.ALL | wx.GROW)

    def _init_coll_versionSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.spinCtrl1, 0, border=0, flag=0)
        parent.AddWindow(self.spinCtrl2, 0, border=0, flag=0)
        parent.AddWindow(self.spinCtrl3, 0, border=0, flag=0)
        parent.AddWindow(self.spinCtrl4, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.applyVersion, 0, border=0, flag=0)

    def _init_coll_cmdSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.loadScriptButton, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.cvsUpdateButton, 0, border=0, flag=0)
        parent.AddWindow(self.cleanButton, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 16), border=0, flag=0)
        parent.AddWindow(self.runScriptButton, 0, border=0, flag=0)
        parent.AddWindow(self.stopScriptButton, 0, border=0, flag=0)

    def _init_coll_makeBranchSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, proportion=1)
        parent.AddWindow(self.newBranchButton, 0, border=0, flag=0)

    def _init_coll_topLeftSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.scriptsCombo, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.staticTextVersion, 0, border=0, flag=0)
        parent.AddSizer(self.versionSizer, 0, border=10, flag=wx.ALL | wx.GROW)
        parent.AddSizer(self.incVersionSizer, 0, border=10,
              flag=wx.ALL | wx.GROW)
        parent.AddSizer(self.branchSizer, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.GROW)
        parent.AddSizer(self.makeBranchSizer, 0, border=0, flag=wx.GROW)
        parent.AddWindow(self.cleanCopy, 0, border=1, flag=wx.BOTTOM | wx.TOP)
        parent.AddWindow(self.makeAllCheckBox, 0, border=1,
              flag=wx.TOP | wx.BOTTOM)
        parent.AddWindow(self.incredibuild, 0, border=1,
              flag=wx.BOTTOM | wx.TOP)

    def _init_coll_branchSizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.branchLabel, 0, border=5,
              flag=wx.RIGHT | wx.ALIGN_CENTER_VERTICAL)
        parent.AddWindow(self.cvsBranch, 1, border=0,
              flag=wx.ALIGN_CENTER_VERTICAL | wx.GROW)
        parent.AddWindow(self.refreshTagListButton, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.frameSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self.topSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bottomSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.cmdSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self.topLeftSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self.versionSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.incVersionSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.branchSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.makeBranchSizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_frameSizer_Items(self.frameSizer)
        self._init_coll_topSizer_Items(self.topSizer)
        self._init_coll_bottomSizer_Items(self.bottomSizer)
        self._init_coll_cmdSizer_Items(self.cmdSizer)
        self._init_coll_topLeftSizer_Items(self.topLeftSizer)
        self._init_coll_versionSizer_Items(self.versionSizer)
        self._init_coll_incVersionSizer_Items(self.incVersionSizer)
        self._init_coll_branchSizer_Items(self.branchSizer)
        self._init_coll_makeBranchSizer_Items(self.makeBranchSizer)

        self.background.SetSizer(self.frameSizer)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_MAINFRAME, name=u'MainFrame',
              parent=prnt, pos=wx.Point(451, 380), size=wx.Size(749, 610),
              style=wx.DEFAULT_FRAME_STYLE, title=u'Build Scripts Manager')
        self.SetClientSize(wx.Size(741, 576))
        self.SetIcon(wx.Icon(u'images/ScriptManager.ico',wx.BITMAP_TYPE_ICO))
        self.Bind(EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_CLOSE, self.OnMainFrameClose)

        self.background = wx.Panel(id=wxID_MAINFRAMEBACKGROUND,
              name=u'background', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(741, 576), style=wx.TAB_TRAVERSAL)
        self.background.SetToolTipString(u'Build Scrips Manager')

        self.scriptsCombo = wx.ComboBox(choices=[],
              id=wxID_MAINFRAMESCRIPTSCOMBO, name=u'scriptsCombo',
              parent=self.background, pos=wx.Point(10, 10), size=wx.Size(298,
              21), style=wx.CB_READONLY | wx.CB_SORT, value=u'')
        self.scriptsCombo.SetLabel(u'')
        self.scriptsCombo.SetToolTipString(u'Select script to run')
        self.scriptsCombo.Bind(EVT_COMBOBOX, self.OnLoadScript,
              id=wxID_MAINFRAMESCRIPTSCOMBO)

        self.quitButton = wx.Button(id=wxID_MAINFRAMEQUITBUTTON, label=u'Quit',
              name=u'quitButton', parent=self.background, pos=wx.Point(656,
              543), size=wx.Size(75, 23), style=0)
        self.quitButton.SetToolTipString(u'Close the manager')
        self.quitButton.SetDefault()
        self.quitButton.Bind(EVT_BUTTON, self.OnQuit,
              id=wxID_MAINFRAMEQUITBUTTON)

        self.stepsList = wx.CheckListBox(choices=[], id=wxID_MAINFRAMESTEPSLIST,
              name=u'stepsList', parent=self.background, pos=wx.Point(333, 10),
              size=wx.Size(303, 225), style=0)
        self.stepsList.SetToolTipString(u'Choose the steps to run')
        self.stepsList.Bind(wx.EVT_CHECKLISTBOX, self.OnStepsListChecklistbox,
              id=wxID_MAINFRAMESTEPSLIST)

        self.runScriptButton = wx.Button(id=wxID_MAINFRAMERUNSCRIPTBUTTON,
              label=u'Start', name=u'runScriptButton', parent=self.background,
              pos=wx.Point(656, 111), size=wx.Size(75, 24), style=0)
        self.runScriptButton.SetToolTipString(u'Run selected script')
        self.runScriptButton.Bind(EVT_BUTTON, self.OnRunScript,
              id=wxID_MAINFRAMERUNSCRIPTBUTTON)

        self.stopScriptButton = wx.Button(id=wxID_MAINFRAMESTOPSCRIPTBUTTON,
              label=u'Stop', name=u'stopScriptButton', parent=self.background,
              pos=wx.Point(656, 135), size=wx.Size(75, 23), style=0)
        self.stopScriptButton.Enable(False)
        self.stopScriptButton.SetToolTipString(u'Stop the running script')
        self.stopScriptButton.Bind(EVT_BUTTON, self.OnStopScript,
              id=wxID_MAINFRAMESTOPSCRIPTBUTTON)

        self.scriptProgress = wx.Gauge(id=wxID_MAINFRAMESCRIPTPROGRESS,
              name=u'scriptProgress', parent=self.background, pos=wx.Point(10,
              355), range=100, size=wx.Size(721, 16), style=wx.GA_HORIZONTAL)

        self.logger = wx.TextCtrl(id=wxID_MAINFRAMELOGGER, name=u'logger',
              parent=self.background, pos=wx.Point(10, 381), size=wx.Size(721,
              142), style=wx.TE_RICH2 | wx.TE_MULTILINE | wx.TE_READONLY,
              value=u'')

        self.loadScriptButton = wx.Button(id=wxID_MAINFRAMELOADSCRIPTBUTTON,
              label=u'Load', name=u'loadScriptButton', parent=self.background,
              pos=wx.Point(656, 10), size=wx.Size(75, 23), style=0)
        self.loadScriptButton.Bind(EVT_BUTTON, self.OnLoadScript,
              id=wxID_MAINFRAMELOADSCRIPTBUTTON)

        self.spinCtrl1 = wx.SpinCtrl(id=wxID_MAINFRAMESPINCTRL1, initial=0,
              max=1000, min=0, name='spinCtrl1', parent=self.background,
              pos=wx.Point(20, 62), size=wx.Size(44, 21),
              style=wx.SP_ARROW_KEYS)
        self.spinCtrl1.SetValue(0)
        self.spinCtrl1.SetToolTipString(u'Major number')
        self.spinCtrl1.Bind(EVT_SPINCTRL, self.OnSpinCtrl,
              id=wxID_MAINFRAMESPINCTRL1)

        self.spinCtrl2 = wx.SpinCtrl(id=wxID_MAINFRAMESPINCTRL2, initial=0,
              max=1000, min=0, name='spinCtrl2', parent=self.background,
              pos=wx.Point(64, 62), size=wx.Size(44, 21),
              style=wx.SP_ARROW_KEYS)
        self.spinCtrl2.SetValue(0)
        self.spinCtrl2.SetToolTipString(u'Minor number')
        self.spinCtrl2.Bind(EVT_SPINCTRL, self.OnSpinCtrl,
              id=wxID_MAINFRAMESPINCTRL2)

        self.spinCtrl3 = wx.SpinCtrl(id=wxID_MAINFRAMESPINCTRL3, initial=0,
              max=1000, min=0, name='spinCtrl3', parent=self.background,
              pos=wx.Point(108, 62), size=wx.Size(44, 21),
              style=wx.SP_ARROW_KEYS)
        self.spinCtrl3.SetValue(0)
        self.spinCtrl3.SetToolTipString(u'Release number')
        self.spinCtrl3.Bind(EVT_SPINCTRL, self.OnSpinCtrl)

        self.spinCtrl4 = wx.SpinCtrl(id=wxID_MAINFRAMESPINCTRL4, initial=0,
              max=1000, min=0, name='spinCtrl4', parent=self.background,
              pos=wx.Point(152, 62), size=wx.Size(44, 21),
              style=wx.SP_ARROW_KEYS)
        self.spinCtrl4.SetValue(0)
        self.spinCtrl4.SetToolTipString(u'Build number')
        self.spinCtrl4.Bind(EVT_SPINCTRL, self.OnSpinCtrl,
              id=wxID_MAINFRAMESPINCTRL4)

        self.staticTextVersion = wx.StaticText(id=wxID_MAINFRAMESTATICTEXTVERSION,
              label=u'Version', name=u'staticTextVersion',
              parent=self.background, pos=wx.Point(10, 39), size=wx.Size(35,
              13), style=0)

        self.applyVersion = wx.Button(id=wxID_MAINFRAMEAPPLYVERSION,
              label=u'Apply', name=u'applyVersion', parent=self.background,
              pos=wx.Point(204, 62), size=wx.Size(75, 23), style=0)
        self.applyVersion.SetToolTipString(u'Apply version changes')
        self.applyVersion.Bind(EVT_BUTTON, self.OnApplyVersionButton,
              id=wxID_MAINFRAMEAPPLYVERSION)

        self.incMajorButton = wx.Button(id=wxID_MAINFRAMEINCMAJORBUTTON,
              label=u'Inc Major', name=u'incMajorButton',
              parent=self.background, pos=wx.Point(20, 105), size=wx.Size(75,
              23), style=0)
        self.incMajorButton.Bind(EVT_BUTTON, self.OnIncMajorButton,
              id=wxID_MAINFRAMEINCMAJORBUTTON)

        self.incMinorButton = wx.Button(id=wxID_MAINFRAMEINCMINORBUTTON,
              label=u'Inc Minor', name=u'incMinorButton',
              parent=self.background, pos=wx.Point(124, 105), size=wx.Size(75,
              23), style=0)
        self.incMinorButton.Bind(EVT_BUTTON, self.OnIncMinorButton,
              id=wxID_MAINFRAMEINCMINORBUTTON)

        self.incReleaseButton = wx.Button(id=wxID_MAINFRAMEINCRELEASEBUTTON,
              label=u'Inc Release', name=u'incReleaseButton',
              parent=self.background, pos=wx.Point(228, 105), size=wx.Size(75,
              23), style=0)
        self.incReleaseButton.Bind(EVT_BUTTON, self.OnIncReleaseButton,
              id=wxID_MAINFRAMEINCRELEASEBUTTON)

        self.messageTextCtrl = wx.TextCtrl(id=wxID_MAINFRAMEMESSAGETEXTCTRL,
              name=u'messageTextCtrl', parent=self.background, pos=wx.Point(10,
              278), size=wx.Size(721, 57), style=wx.TE_MULTILINE, value=u'')
        self.messageTextCtrl.SetToolTipString(u'SVN Log')

        self.messageHistory = wx.Choice(choices=[],
              id=wxID_MAINFRAMEMESSAGEHISTORY, name=u'messageHistory',
              parent=self.background, pos=wx.Point(10, 255), size=wx.Size(721,
              21), style=0)
        self.messageHistory.SetToolTipString(u'SVN Log History')
        self.messageHistory.Bind(EVT_CHOICE, self.OnMessageHistoryChoice,
              id=wxID_MAINFRAMEMESSAGEHISTORY)

        self.saveLog = wx.Button(id=wxID_MAINFRAMESAVELOG, label=u'Save log',
              name=u'saveLog', parent=self.background, pos=wx.Point(10, 543),
              size=wx.Size(75, 23), style=0)
        self.saveLog.Bind(EVT_BUTTON, self.OnSaveLogButton,
              id=wxID_MAINFRAMESAVELOG)

        self.cvsBranch = wx.ComboBox(choices=[], id=wxID_MAINFRAMECVSBRANCH,
              name=u'cvsBranch', parent=self.background, pos=wx.Point(73, 142),
              size=wx.Size(220, 21),
              style=wx.CB_READONLY | wx.CB_DROPDOWN, value=u'')
        self.cvsBranch.SetToolTipString(u'If non empty and valid, the content of this ComboBox will be used to tag sources files onto CVS.')
        self.cvsBranch.SetLabel(u'')
        self.cvsBranch.Enable(False)

        self.branchLabel = wx.StaticText(id=wxID_MAINFRAMEBRANCHLABEL,
              label=u'SVN Branch', name=u'branchLabel', parent=self.background,
              pos=wx.Point(10, 146), size=wx.Size(58, 13), style=0)
        self.branchLabel.Show(True)
        self.branchLabel.Enable(False)

        self.makeAllCheckBox = wx.CheckBox(id=wxID_MAINFRAMEMAKEALLCHECKBOX,
              label=u'Rebuild All', name=u'makeAllCheckBox',
              parent=self.background, pos=wx.Point(10, 206), size=wx.Size(73,
              13), style=0)
        self.makeAllCheckBox.SetValue(False)
        self.makeAllCheckBox.SetToolTipString(u"Force a Rebuild All, even is sources didn't changed")
        self.makeAllCheckBox.Bind(wx.EVT_CHECKBOX, self.OnMakeAllCheckBox,
              id=wxID_MAINFRAMEMAKEALLCHECKBOX)

        self.cvsUpdateButton = wx.Button(id=wxID_MAINFRAMECVSUPDATEBUTTON,
              label=u'SVN Update', name=u'cvsUpdateButton',
              parent=self.background, pos=wx.Point(656, 49), size=wx.Size(75,
              23), style=0)
        self.cvsUpdateButton.Bind(wx.EVT_BUTTON, self.OnCvsUpdateButton,
              id=wxID_MAINFRAMECVSUPDATEBUTTON)

        self.cleanButton = wx.Button(id=wxID_MAINFRAMECLEANBUTTON,
              label=u'Clean', name=u'cleanButton', parent=self.background,
              pos=wx.Point(656, 72), size=wx.Size(75, 23), style=0)
        self.cleanButton.Bind(wx.EVT_BUTTON, self.OnCleanButton,
              id=wxID_MAINFRAMECLEANBUTTON)

        self.newBranchButton = wx.Button(id=wxID_MAINFRAMENEWBRANCHBUTTON,
              label=u'Make New Branch', name=u'newBranchButton',
              parent=self.background, pos=wx.Point(209, 167), size=wx.Size(104,
              23), style=0)
        self.newBranchButton.Bind(wx.EVT_BUTTON, self.OnNewBranchButton,
              id=wxID_MAINFRAMENEWBRANCHBUTTON)

        self.refreshTagListButton = wx.BitmapButton(bitmap=wx.NullBitmap,
              id=wxID_MAINFRAMEREFRESHTAGLISTBUTTON,
              name=u'refreshTagListButton', parent=self.background,
              pos=wx.Point(293, 142), size=wx.Size(20, 20),
              style=wx.BU_AUTODRAW)
        self.refreshTagListButton.SetBitmapLabel(wx.Bitmap(u'images/Refresh.png',
              wx.BITMAP_TYPE_PNG))
        self.refreshTagListButton.Bind(wx.EVT_BUTTON,
              self.OnRefreshTagListButton,
              id=wxID_MAINFRAMEREFRESHTAGLISTBUTTON)

        self.cleanCopy = wx.CheckBox(id=wxID_MAINFRAMECLEANCOPY,
              label=u'SVN get clean copy', name=u'cleanCopy',
              parent=self.background, pos=wx.Point(10, 191), size=wx.Size(111,
              13), style=0)
        self.cleanCopy.SetValue(False)
        self.cleanCopy.Bind(wx.EVT_CHECKBOX, self.OnCleanCopyCheckbox,
              id=wxID_MAINFRAMECLEANCOPY)

        self.incredibuild = wx.CheckBox(id=wxID_MAINFRAMEINCREDIBUILD,
              label=u'Use IncrediBuild', name=u'incredibuild',
              parent=self.background, pos=wx.Point(10, 221), size=wx.Size(95,
              13), style=0)
        self.incredibuild.SetValue(False)
        self.incredibuild.Bind(wx.EVT_CHECKBOX, self.OnIncredibuildCheckbox,
              id=wxID_MAINFRAMEINCREDIBUILD)

        self.Options = wx.Button(id=wxID_MAINFRAMEOPTIONS, label=u'Options',
              name=u'Options', parent=self.background, pos=wx.Point(333, 543),
              size=wx.Size(75, 23), style=0)
        self.Options.Bind(wx.EVT_BUTTON, self.OnOptionsButton,
              id=wxID_MAINFRAMEOPTIONS)

        self._init_sizers()

    def __init__(self, parent):
        self.config = wx.ConfigBase_Get()
        self._init_ctrls(parent)
        self.iconNormal = wx.Icon(u'images/ScriptManager.ico', wx.BITMAP_TYPE_ICO)
        self.iconExe = wx.Icon(u'images/ScriptManagerExe.ico', wx.BITMAP_TYPE_ICO)
        self.frameSizer.SetSizeHints(self.background)
        sys.stdout = self
        self.errLogger = ErrHandler(self.logger)
        names = os.listdir('.')
        for name in _scripts.keys():
            #self.scriptsCombo.Append(os.path.split(name)[1])
            self.scriptsCombo.Append(name)
        scriptname = self.config.Read('/Scripts/LastLoaded')
        if len(scriptname) == 0:
            self.scriptsCombo.SetSelection(0)
        else:
            try:
                self.scriptsCombo.SetStringSelection(scriptname)
            except:
                self.scriptsCombo.SetSelection(0)
        self.thread = None
        self.runScriptButton.Disable()
        self.cleanCopy.SetValue(self.config.ReadBool('/Options/GetCleanCopy', False))
        self.makeAllCheckBox.SetValue(self.config.ReadBool('/Options/MakeAll', False))
        self.incredibuild.SetValue(self.config.ReadBool('/Options/UsingIncrediBuild', False))

        # SVN Log history
        self.LoadMessageHistory()

        # Taskbar icon
##        if wx.Platform == '__WXMSW__':
##            self.tbicon = wx.TaskBarIcon()
##            self.tbicon.SetIcon(self.iconNormal, wx.GetApp().GetAppName())
##            EVT_TASKBAR_LEFT_UP(self.tbicon, self.OnTaskBarActivate)
##            EVT_TASKBAR_RIGHT_UP(self.tbicon, self.OnTaskBarMenu)
##            EVT_MENU(self.tbicon, self.TBMENU_RESTORE, self.OnTaskBarActivate)
##            EVT_MENU(self.tbicon, self.TBMENU_STOP, self.OnStopScript)
##            EVT_MENU(self.tbicon, self.TBMENU_CLOSE, self.OnTaskBarClose)
##            EVT_MENU(self.tbicon, self.TBMENU_ABOUT, self.OnTaskBarAbout)
        self.tbicon = None

        if len(self.scriptsCombo.GetStringSelection()) > 0:
            self.OnLoadScript(wx.CommandEvent())

    def OnQuit(self, event):
        self.Close()

    def OnMainFrameClose(self, event):
        if self.tbicon != None:
            self.tbicon.RemoveIcon()
            del self.tbicon
        self.Destroy()

    def OnStepsListChecklistbox(self, event):
        index = event.GetInt()
        text = self.stepsList.GetString(index)
        isChecked = bool(self.stepsList.IsChecked(index))
        self.__steps[text] = isChecked
        self.config.WriteBool('/BuildSteps/' + text, isChecked)
##        print text + ' = ' + str(isChecked)

    def write(self, txt):
        pos = self.logger.GetInsertionPoint()
        if type(txt) is str:
##            self.logger.write(repr(txt))
            self.logger.write(txt.decode('cp850'))
        else:
##            self.logger.write('Type = %s' % repr(type(txt)))
            self.logger.write(txt)
        endPos = self.logger.GetInsertionPoint()
        color = None
        if txt[:3] == '***':
            textAttr = self.logger.GetDefaultStyle()
            font = textAttr.GetFont()
            font.SetWeight(wx.BOLD)
            attr = wx.TextAttr(font = font)
            self.logger.SetStyle(pos, endPos, attr)
        if txt[:2] == 'P ':
            color = wx.BLUE
            textWeight = wx.NORMAL
        if txt[:2] == 'U ':
            color = wx.GREEN
            textWeight = wx.NORMAL
        if txt[:2] == 'T ':
            color = wx.GREEN
            textWeight = wx.NORMAL
        if txt[:2] == 'C ':
            color = wx.RED
            textWeight = wx.BOLD
        if txt[:2] == 'M ':
            color = wx.CYAN
            textWeight = wx.NORMAL
        if txt[:2] == '? ':
            color = wx.LIGHT_GREY
            textWeight = wx.NORMAL
        if color != None:
            textAttr = self.logger.GetDefaultStyle()
            font = textAttr.GetFont()
            font.SetWeight(textWeight)
            attr = wx.TextAttr(font = font)
            attr = wx.TextAttr(colText = color, font = font)
            self.logger.SetStyle(pos, endPos, attr)

    def OnLoadScript(self, event):
        reload(Script)
        scriptname = _scripts[self.scriptsCombo.GetStringSelection()]
        filename = scriptname  # + '.xml'
#        file = open(filename, 'rt')
#        self.plugin = load_source(scriptname, filename, file)
#        file.close()
        try:
            self.script = Script.Script(filename, self.AuthentificationCallback)
            self.__steps.clear()
            self.stepsList.Clear()
            i = 0
            for key in Script._steps:
                self.__steps[key] = False
            for name in self.script.runStepsNames:
                self.__steps[name] = self.config.ReadBool('/BuildSteps/' + name, True)
                self.stepsList.Append(unicode(name))
                self.stepsList.Check(i, self.__steps[name])
                i += 1
            try:
                tag = self.script.getCurrentTagName()
            except Exception, e:
                if hasattr(e, 'args'):
                    if isinstance(e.args, str):
                        msg = e.args
                    elif isinstance(e.args, tuple) and len(e.args) > 0 and isinstance(e.args[0], str):
                        msg = e.args[0]
                    else:
                        msg = str(e)
                else:
                    msg = str(e)
                tag = None
                wx.MessageBox(msg, "Error getting current tag name...", wx.ICON_ERROR)
            if tag:
                self.cvsBranch.Clear()
                self.cvsBranch.Append(tag)
                self.cvsBranch.Enable(True)
                self.cvsBranch.SetValue(tag)
                self.branchLabel.Enable(True)
                self.newBranchButton.Enable(True)
            else:
                self.cvsBranch.Enable(False)
                self.cvsBranch.SetValue('')
                self.branchLabel.Enable(False)
                self.newBranchButton.Enable(False)
                self.cvsBranch.Clear()
            self.UpdateButtonState()
            self.config.Write('/Scripts/LastLoaded', self.scriptsCombo.GetStringSelection())
        except Exception, detail:
            wx.LogError(str(sys.exc_info()[0]) + ' : ' + str(detail) + '\n')
            wx.LogError('   Traceback:\n')
            tb = sys.exc_info()[2]
            callList = traceback.format_tb(tb)
            for line in callList:
                wx.LogError(line)
            wx.LogError('File [%s] seems to not been a valid build script.' % (filename,))
            self.plugin = None
            self.script = None
            self.logger.SetFocus()
            return
        self.LoadVersion()
        self.logger.SetFocus()

    def LoadVersion(self):
        self.spinCtrl1.Disable()
        self.spinCtrl2.Disable()
        self.spinCtrl3.Disable()
        self.spinCtrl4.Disable()
        self.applyVersion.Enable(False)
        self.incMajorButton.Enable(False)
        self.incMinorButton.Enable(False)
        self.incReleaseButton.Enable(False)
        if self.script != None:
            try:
                self.version = self.script.getVersion()
            except:
                return
            self.spinCtrl1.SetValue(self.version[0])
            self.spinCtrl2.SetValue(self.version[1])
            self.spinCtrl3.SetValue(self.version[2])
            self.spinCtrl4.SetValue(self.version[3])
            self.spinCtrl1.Enable(True)
            self.spinCtrl2.Enable(True)
            self.spinCtrl3.Enable(True)
            self.spinCtrl4.Enable(True)
            self.applyVersion.Disable()
            self.modifiedVersion = False
            self.incMajorButton.Enable(True)
            self.incMinorButton.Enable(True)
            self.incReleaseButton.Enable(True)

    def OnCvsUpdateButton(self, event):
        self.logger.Clear()
        self.logger.SetFocus()
        tmpStep = {}
        for key in Script._steps:
            tmpStep[key] = False
        tmpStep['getSources'] = True
        cvsTag = self.GetSelectedTag()
        print 'Starting executing SVN Update on ' + self.script.name
        self.thread = Thread(name = 'Build Script', target = self.script.run,
                             args = (tmpStep,),
                             kwargs = { 'makeAll': self.makeAllCheckBox.IsChecked(),
                                        'cvsTag' : cvsTag,
                                        'cvsClean': self.cleanCopy.IsChecked()} )
        sys.stdout = self
        sys.stderr = self.errLogger
        self.thread.start()
        self.UpdateButtonState()

    def OnRunScript(self, event):
        self.logger.Clear()
        self.logger.SetFocus()
        print 'Starting executing ' + self.script.name
        message = self.messageTextCtrl.GetValue()
        if len(message) == 0:
            message = 'Autobuild commit for version %s' % str(self.version)
        else:
            self.AddMessageToHistory(message)
        cvsTag = self.GetSelectedTag()
        self.thread = Thread(name = 'Build Script', target = self.script.run,
                             args = (self.__steps,),
                             kwargs = { 'cvsLog' : message,
                                        'cvsTag' : cvsTag,
                                        'makeAll': self.makeAllCheckBox.IsChecked(),
                                        'cvsClean': self.cleanCopy.IsChecked(),
                                        'incredibuild': self.incredibuild.IsChecked()} )
        sys.stdout = self
        sys.stderr = self.errLogger
        self.thread.start()
        self.UpdateButtonState()

    def OnStopScript(self, event):
        self.logger.SetFocus()
        try:
            self.script.kill()
##            self.thread.join(10)
        finally:
            sys.stderr = ErrorLoggerPF()
            self.thread = None
            self.UpdateButtonState()
            self.LoadVersion()

    def OnIdle(self, event):
        if self.thread != None:
            if not self.thread.isAlive():   # thread finished ?
                sys.stderr = ErrorLoggerPF()
                self.thread = None
                self.UpdateButtonState()
                self.LoadVersion()
        event.Skip()

    def UpdateButtonState(self):
        if self.thread == None:
            self.loadScriptButton.Enable(True)
            self.cvsUpdateButton.Enable(True)
            self.cleanButton.Enable(True)
            self.runScriptButton.Enable(True)
            self.refreshTagListButton.Enable(True)
            self.scriptsCombo.Enable(True)
            self.newBranchButton.Enable(True)
            self.cleanCopy.Enable(True)
            self.makeAllCheckBox.Enable(True)
            self.incredibuild.Enable(True)
            self.stopScriptButton.Disable()
            if self.tbicon != None:
                self.tbicon.SetIcon(self.iconNormal)
        else:
            self.loadScriptButton.Disable()
            self.cvsUpdateButton.Disable()
            self.cleanButton.Disable()
            self.runScriptButton.Disable()
            self.refreshTagListButton.Disable()
            self.scriptsCombo.Disable()
            self.spinCtrl1.Disable()
            self.spinCtrl2.Disable()
            self.spinCtrl3.Disable()
            self.spinCtrl4.Disable()
            self.applyVersion.Disable()
            self.incMajorButton.Disable()
            self.incMinorButton.Disable()
            self.incReleaseButton.Disable()
            self.newBranchButton.Disable()
            self.cleanCopy.Disable()
            self.makeAllCheckBox.Disable()
            self.incredibuild.Disable()
            self.stopScriptButton.Enable(True)
            if self.tbicon != None:
                self.tbicon.SetIcon(self.iconExe)


    [TBMENU_RESTORE, TBMENU_STOP, TBMENU_CLOSE, TBMENU_ABOUT] = map(lambda x: wx.NewId(), range(4))

    def OnTaskBarMenu(self, event):
        menu = wx.Menu()
        if self.thread != None:
            menu.Append(self.TBMENU_STOP, 'Stop script execution')
            menu.AppendSeparator()
        if not self.IsShown():
            menu.Append(self.TBMENU_RESTORE, 'Restore ' + wx.GetApp().GetAppName())
        else:
            menu.Append(self.TBMENU_RESTORE, 'Hide ' + wx.GetApp().GetAppName())
        menu.Append(self.TBMENU_CLOSE,   'Exit')
        menu.Append(-1, '')
        menu.Append(self.TBMENU_ABOUT,   'About')
        self.tbicon.PopupMenu(menu)
        menu.Destroy()

    def OnTaskBarActivate(self, event):
        if not self.IsShown():
            self.Show()
        else:
            self.Hide()

    def OnTaskBarClose(self, event):
        self.Close()
        wx.GetApp().ProcessIdle()

    def OnTaskBarAbout(self, event):
        self.OnHelpMenuAboutMenu(event)

    def OnSpinCtrl(self, event):
        self.applyVersion.Enable(True)
        event.Skip()

    def OnIncMajorButton(self, event):
        self.version.inc_major()
        if wx.MessageBox('New version will be %s\nAre you sure?' % str(self.version), 'Increment product version', wx.ICON_QUESTION | wx.YES_NO) == wx.YES:
            self.version.write_version()
        self.LoadVersion()

    def OnIncMinorButton(self, event):
        self.version.inc_minor()
        if wx.MessageBox('New version will be %s\nAre you sure?' % str(self.version), 'Increment product version', wx.ICON_QUESTION | wx.YES_NO) == wx.YES:
            self.version.write_version()
        self.LoadVersion()

    def OnIncReleaseButton(self, event):
        self.version.inc_release()
        if wx.MessageBox('New version will be %s\nAre you sure?' % str(self.version), 'Increment product version', wx.ICON_QUESTION | wx.YES_NO) == wx.YES:
            self.version.write_version()
        self.LoadVersion()

    def OnApplyVersionButton(self, event):
        self.version[0] = self.spinCtrl1.GetValue()
        self.version[1] = self.spinCtrl2.GetValue()
        self.version[2] = self.spinCtrl3.GetValue()
        self.version[3] = self.spinCtrl4.GetValue()
        if wx.MessageBox('New version will be %s\nAre you sure?' % str(self.version), 'Increment product version', wx.ICON_QUESTION | wx.YES_NO) == wx.YES:
            self.version.write_version()
        self.LoadVersion()

    def OnSaveLogButton(self, event):
        event.Skip()

    def OnMessageHistoryChoice(self, event):
        i = self.messageHistory.GetSelection()
        message = wx.ConfigBase_Get().Read('/History/%d' % i, '')
        self.messageTextCtrl.SetValue(message)

    def LoadMessageHistory(self):
        historyLimit = wx.ConfigBase_Get().ReadInt('/History/Limit', 15)
        self.messageHistory.Clear()
        for i in range(historyLimit):
            msg = wx.ConfigBase_Get().Read('/History/%d' % i, '')
            if msg == '':
                break
            if len(msg) > 120:
                self.messageHistory.Append(msg[:117] + '...')
            else:
                self.messageHistory.Append(msg)
        self.messageHistory.SetSelection(0)

    def AddMessageToHistory(self, message):
        if message == '' or message == None:
            return
        index = self.messageHistory.FindString(message)
        if index >= 0:
            self.messageHistory.Delete(index)
        historyLimit = wx.ConfigBase_Get().ReadInt('/History/Limit', 15)

        while self.messageHistory.GetCount() > historyLimit - 1:
            self.messageHistory.Delete(self.messageHistory.GetCount() - 1)
        for i in range(self.messageHistory.GetCount()-1, -1, -1):
            wx.ConfigBase_Get().Write('/History/%d' % (i + 1), wx.ConfigBase_Get().Read('/History/%d' % i, ''))
        wx.ConfigBase_Get().Write('/History/0', message)
        self.messageHistory.Clear()
        self.LoadMessageHistory()

    def OnCleanCopyCheckbox(self, event):
        self.config.WriteBool('/Options/GetCleanCopy', self.cleanCopy.IsChecked())

    def OnMakeAllCheckBox(self, event):
        self.config.WriteBool('/Options/MakeAll', self.makeAllCheckBox.IsChecked())

    def OnIncredibuildCheckbox(self, event):
        self.config.WriteBool('/Options/UsingIncrediBuild', self.incredibuild.IsChecked())

    def OnCleanButton(self, event):
        dialog = ProjectDialog.ProjectsDialog(self)
        dialog.SetTitle(u'Select projects to clean')
        dialog.okButton.SetLabel(u'Clean')
        for name, project in self.script.projects.items():
            dialog.projectsList.Append(name)
        ret = dialog.ShowModal()
        if ret == wx.ID_OK:
            projects = []
            items = dialog.projectsList.GetSelections()
            for item in range(dialog.projectsList.GetCount()):
                if dialog.projectsList.IsChecked(item):
                    projects.append(dialog.projectsList.GetString(item))
            self.thread = Thread(name = 'Build Script', target = self.script.cleanProjects,
                                 args = (projects,),
                                 kwargs = { 'projectOnly': True} )
            sys.stdout = self
            sys.stderr = self.errLogger
            self.thread.start()
            self.UpdateButtonState()

    def OnNewBranchButton(self, event):
        self.logger.SetFocus()
        version = self.script.getVersion()
        strVersion = '%(maj)d_%(min)d'\
                   %  {'maj' : version[0], 'min' : version[1], \
                   'rel' : version[2], 'build' : version[3]}
        tagName = self.GetSelectedTag()
        if tagName == None:
            tageName = 'UNNAMED'
        list = tagName.split('_')
        if len(list) > 4:
            branchName = list[0]
            for item in list[1:len(list)-2]:
                branchName += '_' + item
            branchName += '_BRANCH'
        else:
            branchName = tagName + '_BRANCH'
        dialog = wx.TextEntryDialog(self, 'Do you realy want to create a new BRANCH\nfrom the tag \'' + tagName + '\'?\n\nPlease enter the name of the new Branch:',
                                   'Please enter the BRANCH name', branchName)
        if dialog.ShowModal() == wx.ID_OK:
            branchName = dialog.GetValue()
            print 'Making ' + branchName
            self.logger.Clear()
            self.thread = Thread(name = 'Greating branch', target = self.script.makeBranch,
                                 args = (tagName, branchName))
            sys.stdout = self
            sys.stderr = self.errLogger
            self.thread.start()
            self.UpdateButtonState()

    def OnRefreshTagListButton(self, event):
        self.logger.Clear()
        self.logger.SetFocus()
        print 'Starting executing ' + self.script.name
        self.thread = Thread(name = 'Getting logs', target = self.RefreshTagListThread)
        sys.stdout = self
        sys.stderr = self.errLogger
        self.thread.start()
        self.UpdateButtonState()

    def RefreshTagListThread(self):
        tags, branches = self.script.getTagList()
        self.newBranchButton.Enable(False)
        self.cvsBranch.Clear()
        self.cvsBranch.Append('')
        self.cvsBranch.Append('HEAD / Main trunk')
        self.cvsBranch.Append('--- TAGS ---')
        for tag in tags:
            self.cvsBranch.Append(tag)
        self.cvsBranch.Append('--- BRANCHES ---')
        for tag in branches:
            self.cvsBranch.Append(tag)
        tag = self.script.getCurrentTagName()
        if tag:
            print tag
            self.cvsBranch.SetValue(tag)
        else:
            self.cvsBranch.SetSelection(1)
        self.cvsBranch.Enable(True)
        self.branchLabel.Enable(True)
        self.newBranchButton.Enable(True)

    def OnOptionsButton(self, event):
        dialog = OptionsDialog.OptionsDialog(self)
        dialog.ShowModal()

    def AuthentificationCallback(self, realm, username, may_save):
        config = wx.ConfigBase_Get()
        print 'Ask for authentification...'
        user = config.Read('/Options/Authentification/User', '').encode('latin')
        crypt = Blowfish.new('SoftArchi', Blowfish.MODE_ECB)
        #crypt = blowfish.Blowfish('SoftArchi')
        pwd = crypt.decrypt(config.Read('/Options/Authentification/Password', '').encode('latin'))
        if len(pwd) > 0:
            print 'True...'
            return True, user, pwd, True
        else:
            print 'False...'
            return False, user, pwd, False

    def GetSelectedTag(self):
        if self.cvsBranch.IsEnabled():
            index = self.cvsBranch.GetSelection()
            if index == 0 or index == 1:
                cvsTag = ''
            else:
                cvsTag = self.cvsBranch.GetValue()
        else:
            cvsTag = ''
        if cvsTag == '' or cvsTag.upper() == 'HEAD' or cvsTag.upper() == 'TRUNK' or cvsTag[:3] == '---':
            cvsTag = None
        return cvsTag