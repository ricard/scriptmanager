#Boa:Dialog:ProjectsDialog

import wx

def create(parent):
    return ProjectsDialog(parent)

[wxID_PROJECTSDIALOG, wxID_PROJECTSDIALOGCANCELBUTTON, 
 wxID_PROJECTSDIALOGOKBUTTON, wxID_PROJECTSDIALOGPROJECTSLIST, 
] = [wx.NewId() for _init_ctrls in range(4)]

class ProjectsDialog(wx.Dialog):
    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.projectsList, 1, border=10, flag=wx.ALL | wx.GROW)
        parent.AddSizer(self.boxSizer2, 0, border=10,
              flag=wx.ALIGN_CENTER_HORIZONTAL | wx.ALL)

    def _init_coll_boxSizer2_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.okButton, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.cancelButton, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)
        self._init_coll_boxSizer2_Items(self.boxSizer2)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_PROJECTSDIALOG, name=u'ProjectsDialog',
              parent=prnt, pos=wx.Point(698, 251), size=wx.Size(270, 250),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Projects selector')
        self.SetClientSize(wx.Size(262, 216))
        self.SetToolTipString(u'Projects selector')

        self.projectsList = wx.CheckListBox(choices=[],
              id=wxID_PROJECTSDIALOGPROJECTSLIST, name=u'projectsList',
              parent=self, pos=wx.Point(10, 10), size=wx.Size(242, 153),
              style=0)
        self.projectsList.SetMinSize(wx.Size(100, 80))

        self.okButton = wx.Button(id=wxID_PROJECTSDIALOGOKBUTTON, label=u'Ok',
              name=u'okButton', parent=self, pos=wx.Point(52, 183),
              size=wx.Size(75, 23), style=0)
        self.okButton.SetDefault()
        self.okButton.Bind(wx.EVT_BUTTON, self.OnOk, id=wxID_PROJECTSDIALOGOKBUTTON)

        self.cancelButton = wx.Button(id=wxID_PROJECTSDIALOGCANCELBUTTON,
              label=u'Cancel', name=u'cancelButton', parent=self,
              pos=wx.Point(135, 183), size=wx.Size(75, 23), style=0)
        self.cancelButton.Bind(wx.EVT_BUTTON, self.OnCancel, id=wxID_PROJECTSDIALOGCANCELBUTTON)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)

    def OnOk(self, event):
        self.EndModal(wx.ID_OK)
        
    def OnCancel(self, event):
        self.EndModal(wx.ID_CANCEL)
        
        