#Boa:Dialog:OptionsDialog

import wx
#import blowfish
from Crypto.Cipher import Blowfish

def create(parent):
    return OptionsDialog(parent)

[wxID_OPTIONSDIALOG, wxID_OPTIONSDIALOGCANCELBUTTON,
 wxID_OPTIONSDIALOGNOTEBOOK1, wxID_OPTIONSDIALOGOKBUTTON,
 wxID_OPTIONSDIALOGPANEL1, wxID_OPTIONSDIALOGPASSWORDTEXTCTRL,
 wxID_OPTIONSDIALOGPASSWORDTEXTCTRL2, wxID_OPTIONSDIALOGSTATICTEXT1,
 wxID_OPTIONSDIALOGSTATICTEXT2, wxID_OPTIONSDIALOGSTATICTEXT3,
 wxID_OPTIONSDIALOGUSERTEXTCTRL,
] = [wx.NewId() for _init_ctrls in range(11)]

class OptionsDialog(wx.Dialog):
    def _init_coll_gridBagSizer1_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_flexGridSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.staticText1, 0, border=0,
              flag=wx.ALIGN_CENTER_VERTICAL)
        parent.AddWindow(self.userTextCtrl, 0, border=0, flag=0)
        parent.AddWindow(self.staticText2, 0, border=0,
              flag=wx.ALIGN_CENTER_VERTICAL)
        parent.AddWindow(self.passwordTextCtrl, 0, border=0, flag=0)
        parent.AddWindow(self.staticText3, 0, border=0,
              flag=wx.ALIGN_CENTER_VERTICAL)
        parent.AddWindow(self.passwordTextCtrl2, 0, border=0, flag=0)

    def _init_coll_gridBagSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.notebook1, (0, 0), border=10,
              flag=wx.GROW | wx.ALL, span=(1, 3))
        parent.AddSpacer(wx.Size(8, 8), (1, 0), border=0, flag=0, span=(1, 1))
        parent.AddWindow(self.okButton, (1, 1), border=5, flag=wx.ALL, span=(1,
              1))
        parent.AddWindow(self.cancelButton, (1, 2), border=5, flag=wx.ALL,
              span=(1, 1))

    def _init_coll_notebook1_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.panel1, select=True,
              text=u'Authentification')

    def _init_sizers(self):
        # generated method, don't edit
        self.gridBagSizer1 = wx.GridBagSizer(hgap=5, vgap=5)
        self.gridBagSizer1.SetCols(3)

        self.flexGridSizer1 = wx.FlexGridSizer(cols=2, hgap=5, rows=3, vgap=5)

        self._init_coll_gridBagSizer1_Items(self.gridBagSizer1)
        self._init_coll_gridBagSizer1_Growables(self.gridBagSizer1)
        self._init_coll_flexGridSizer1_Items(self.flexGridSizer1)

        self.SetSizer(self.gridBagSizer1)
        self.panel1.SetSizer(self.flexGridSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_OPTIONSDIALOG, name=u'OptionsDialog',
              parent=prnt, pos=wx.Point(372, 298), size=wx.Size(302, 228),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Options')
        self.SetClientSize(wx.Size(294, 194))

        self.okButton = wx.Button(id=wxID_OPTIONSDIALOGOKBUTTON, label=u'Ok',
              name=u'okButton', parent=self, pos=wx.Point(124, 166),
              size=wx.Size(75, 23), style=0)
        self.okButton.Bind(wx.EVT_BUTTON, self.OnOkButtonButton,
              id=wxID_OPTIONSDIALOGOKBUTTON)

        self.cancelButton = wx.Button(id=wxID_OPTIONSDIALOGCANCELBUTTON,
              label=u'Cancel', name=u'cancelButton', parent=self,
              pos=wx.Point(214, 166), size=wx.Size(75, 23), style=0)
        self.cancelButton.Bind(wx.EVT_BUTTON, self.OnCancelButtonButton,
              id=wxID_OPTIONSDIALOGCANCELBUTTON)

        self.notebook1 = wx.Notebook(id=wxID_OPTIONSDIALOGNOTEBOOK1,
              name='notebook1', parent=self, pos=wx.Point(10, 10),
              size=wx.Size(274, 136), style=0)

        self.panel1 = wx.Panel(id=wxID_OPTIONSDIALOGPANEL1, name='panel1',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(266, 110),
              style=wx.TAB_TRAVERSAL)

        self.staticText1 = wx.StaticText(id=wxID_OPTIONSDIALOGSTATICTEXT1,
              label=u'SVN User', name='staticText1', parent=self.panel1,
              pos=wx.Point(0, 4), size=wx.Size(44, 13), style=0)

        self.userTextCtrl = wx.TextCtrl(id=wxID_OPTIONSDIALOGUSERTEXTCTRL,
              name=u'userTextCtrl', parent=self.panel1, pos=wx.Point(91, 0),
              size=wx.Size(165, 21), style=0, value=u'')

        self.staticText2 = wx.StaticText(id=wxID_OPTIONSDIALOGSTATICTEXT2,
              label=u'SVN Password', name='staticText2', parent=self.panel1,
              pos=wx.Point(0, 30), size=wx.Size(68, 13), style=0)

        self.passwordTextCtrl = wx.TextCtrl(id=wxID_OPTIONSDIALOGPASSWORDTEXTCTRL,
              name=u'passwordTextCtrl', parent=self.panel1, pos=wx.Point(91,
              26), size=wx.Size(165, 21), style=wx.TE_PASSWORD, value=u'')

        self.staticText3 = wx.StaticText(id=wxID_OPTIONSDIALOGSTATICTEXT3,
              label=u'Confirm password', name='staticText3', parent=self.panel1,
              pos=wx.Point(0, 56), size=wx.Size(86, 13), style=0)

        self.passwordTextCtrl2 = wx.TextCtrl(id=wxID_OPTIONSDIALOGPASSWORDTEXTCTRL2,
              name=u'passwordTextCtrl2', parent=self.panel1, pos=wx.Point(91,
              52), size=wx.Size(165, 21), style=wx.TE_PASSWORD, value=u'')

        self._init_coll_notebook1_Pages(self.notebook1)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        self.crypt = Blowfish.new('SoftArchi', Blowfish.MODE_ECB)
        
        config = wx.ConfigBase_Get()
        self.userTextCtrl.SetValue(config.Read('/Options/Authentification/User', ''))
        pwd = self.crypt.decrypt(config.Read('/Options/Authentification/Password', '').encode('latin'))
        self.passwordTextCtrl.SetValue(pwd)
        self.passwordTextCtrl2.SetValue(pwd)
        

    def OnOkButtonButton(self, event):
        config = wx.ConfigBase_Get()
        pwd1 = self.passwordTextCtrl.GetValue()
        pwd2 = self.passwordTextCtrl2.GetValue()

        if pwd1 != pwd2:
            wx.MessageBox('Error: passwords mismatch!\nPlease retype your password.', 'Password mismatch!', wx.ICON_ERROR)
            return
        
        config.Write('/Options/Authentification/User', self.userTextCtrl.GetValue())
        config.Write('/Options/Authentification/Password', self.crypt.encrypt(pwd1))
        self.EndModal(wx.ID_OK)

    def OnCancelButtonButton(self, event):
        self.EndModal(wx.ID_CANCEL)
